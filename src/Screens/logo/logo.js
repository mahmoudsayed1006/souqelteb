import React from 'react';
  import Menu from '../../components/menu/menu';
  import AppMenu from '../../components/menu/menu';
  
  import Nav from '../../components/navbar/navbar';
  import Footer from '../../components/footer/footer';
  import {Carousel} from 'react-materialize';
  import './logo.css';
  import { Skeleton, message,Modal, Form,Spin, Icon, Input, Button,Popconfirm,Select} from 'antd';
  import "antd/dist/antd.css";
  import axios from 'axios';
  import {BASE_END_POINT} from '../../config/URL'
  import { connect } from 'react-redux';
  import  {allStrings} from '../../assets/strings'
  
  const antIcon = <Icon type="loading" style={{ fontdescription: 24 }} spin />;
  const { TextArea } = Input;
  
  
  class Logo extends React.Component {
    page = 0;
    pagentationPage=0;
    counter=0;
    state = {
      modal1Visible: false,
      confirmDelete: false,
       Logo:[],
       file:null,
       loading:true,
       tablePage:0,
       image:""
       }
  
       constructor(props){
        super(props)
        this.getLogo()
        if(this.props.isRTL){
          allStrings.setLanguage('ar')
        }else{
          allStrings.setLanguage('en')
        }
      }
  
       onChange = (e) => {
        this.setState({file:e.target.files[0]});
       
    }
      //submit form
      flag = -1;
      getLogo = () => {
        this.setState({loading:true})
        axios.get(`${BASE_END_POINT}logo`)
        .then(response=>{
          this.setState({Logo:response.data.data[0],loading:false})
          this.setState({image:response.data.data[0].img[0]})
        })
        .catch(error=>{
          //console.log("ALL Categories ERROR")
          //console.log(error.response)
          this.setState({loading:false})
        })
      }
      componentDidMount(){
        this.getLogo()
      }
      
      handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll(['description','arabicDescription'],(err, values)  => {
          if (!err) {
            //console.log('Received values of form: ', values);
            var form = new FormData();
            if(this.state.file){
              form.append('img', this.state.file);
            }
            
            form.append('description', values.description);
            form.append('arabicDescription', values.arabicDescription);
           
      
            let l = message.loading(allStrings.wait, 2.5)
            axios.put(`${BASE_END_POINT}logo/${this.state.Logo.id}`,form,{
              headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${this.props.currentUser.token}`
              },
            })
            .then(response=>{
                l.then(() => message.success(allStrings.updatedDone, 2.5))
                this.setState({ modal1Visible:false });
                this.getLogo()
                this.flag = -1
                this.props.form.resetFields() 
            })
            .catch(error=>{
              console.log(error.response)
              l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
            })
          }
        });
        
      }
       
      //end submit form
  
        setModal1Visible(modal1Visible) {
          this.setState({ modal1Visible });
        }
  
  //end modal
      render() {
          //form
           const { getFieldDecorator } = this.props.form;
           const {Logo,image} = this.state
             const {select} = this.props;
        return (

            <div className='conatiner'>
              {this.props.currentUser&&
                <AppMenu goTo={this.props.history}></AppMenu>            
              }
              {this.props.currentUser&&
                <Nav></Nav>            
              }
               
                <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}}>
                <div className='termsContainerData'>
                    <div className='termsContainer'>
                          <span className='termsTitle'>{allStrings.website}</span>                       
                    </div>
                    {this.state.loading?
                    <Spin indicator={antIcon} />
                    :
                      <span className='termsData'>
                          <img src={image} style={{width:'150px',margin:'auto',marginBottom:'20px'}}/>
                          <p>{Logo.description}</p>
                          <p>{Logo.arabicDescription}</p>
                      </span>
                    }
                </div>
                <br></br>
                <br></br>
                  
                 
                
  
                {this.props.currentUser&&
                  <Button style={{color: 'white', backgroundColor:'#25272e', marginLeft:20,borderRadius: '50%',width:'80px',height:'80px'}} onClick={()=>this.setModal1Visible(true)} >{allStrings.update}</Button>
  
                }
                
                    
                  
                <div>
                <Modal
                      title={allStrings.edit}
                      visible={this.state.modal1Visible}
                      onOk={this.handleSubmit}
                      onCancel={() => this.setModal1Visible(false)}
                      okText={allStrings.ok}
                      cancelText={allStrings.cancel}
                    >
                      <Form onSubmit={this.handleSubmit} className="login-form">
                        <Form.Item>
                          {getFieldDecorator('description', {
                              rules: [{ required: true, message: 'Please enter description' }],
                              initialValue: Logo.description
                          })(
                              <Input  />
                          )}
                          </Form.Item>
                          <Form.Item>
                          {getFieldDecorator('arabicDescription', {
                              rules: [{ required: true, message: 'Please enter arabic description' }],
                              initialValue:Logo.arabicDescription
                          })(
                              <Input  />
                          )}
                        </Form.Item>
                          
                      </Form>
                      <input className='file' type="file" onChange= {this.onChange} multiple></input>
  
                  </Modal>
                </div> 
              </div>
               <Footer></Footer>
            </div>
        );
      }
    }
  
    const mapToStateProps = state => ({
      isRTL: state.lang.isRTL,
      currentUser: state.auth.currentUser,
      select: state.menu.select,
  
    })
    
    const mapDispatchToProps = {
    }
  
    export default  connect(mapToStateProps,mapDispatchToProps)( Logo= Form.create({ name: 'normal_login' })(Logo));
  
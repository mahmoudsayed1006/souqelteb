import React from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import './product info.css';
import {Carousel} from 'react-materialize';
import "antd/dist/antd.css";
 import { Tag,Input, Modal,Form ,Select,message} from 'antd';
 import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux'
import  {allStrings} from '../../assets/strings'
import {Table} from 'react-materialize'

class ProductInfo extends React.Component {
    state = {
         visible: false,
         editModel: false,
         product:this.props.location.state.data,
         file:null,
         once:false,
         categories:[],
         sizes:[],
         Colors: this.props.location.state.data.color,
         colorInput: false,
         colorValue: '',
         sizesCustom: this.props.location.state.data.sizes,
         sizesInput: false,
         sizesValue: '',
         }
         
     handelColorClose = removedTag => {
       const Colors = this.state.Colors.filter(tag => tag !== removedTag);
       console.log(Colors);
       this.setState({ Colors });
     };
   
     showColorInput = () => {
       this.setState({ colorInput: true }, () => this.input.focus());
     };
   
     handelInputColorChange = e => {
       this.setState({ colorValue: e.target.value });
     };
   
     handelInputColorConfirm = () => {
       const { colorValue } = this.state;
       let { Colors } = this.state;
       if (colorValue && Colors.indexOf(colorValue) === -1) {
         Colors = [...Colors, colorValue];
       }
       console.log(Colors);
       this.setState({
         Colors,
         colorInput: false,
         colorValue: '',
       });
     };
   
     saveInputColorRef = input => (this.input = input);
   
     forColorMap = tag => {
       const tagElem = (
         <Tag
           closable
           onClose={e => {
             e.preventDefault();
             this.handelColorClose(tag);
           }}
         >
           {tag}
         </Tag>
       );
       return (
         <span key={tag} style={{ display: 'inline-block' }}>
           {tagElem}
         </span>
       );
     };
   //
   handelsizesClose = removedTag => {
     const sizesCustom = this.state.sizesCustom.filter(tag => tag !== removedTag);
     console.log(sizesCustom);
     this.setState({ sizesCustom });
   };
   
   showsizesInput = () => {
     this.setState({ sizesInput: true }, () => this.input.focus());
   };
   
   handelInputsizesChange = e => {
     this.setState({ sizesValue: e.target.value });
   };
   
   handelInputsizesConfirm = () => {
     const { sizesValue } = this.state;
     let { sizesCustom } = this.state;
     if (sizesValue && sizesCustom.indexOf(sizesValue) === -1) {
       sizesCustom = [...sizesCustom, sizesValue];
     }
     console.log(sizesCustom);
     this.setState({
       sizesCustom,
       sizesInput: false,
       sizesValue: '',
     });
   };
   
   saveInputsizesRef = input => (this.input = input);
   
   forsizesMap = tag1 => {
     const tagElem1 = (
       <Tag
         closable
         onClose={e => {
           e.preventDefault();
           this.handelsizesClose(tag1);
         }}
       >
         {tag1}
       </Tag>
     );
     return (
       <span key={tag1} style={{ display: 'inline-block' }}>
         {tagElem1}
       </span>
     );
   };

         constructor(props){
          super(props)
          console.log(this.props.location.state.data.color)
          if(this.props.isRTL){
            allStrings.setLanguage('ar')
          }else{
            allStrings.setLanguage('en')
          }
        }

         getCategories = () => {
            axios.get(`${BASE_END_POINT}categories`)
            .then(response=>{
             // console.log("ALL Categories")
              //console.log(response.data)
              this.setState({categories:response.data.data})
            })
            .catch(error=>{
              //console.log("ALL Categories ERROR")
              //console.log(error.response)
            })
          }
          getSize = (page) => {
            axios.get(`${BASE_END_POINT}size`)
            .then(response=>{
            //  console.log("ALL Categories")
           // console.log(response.data)
         
              this.setState({sizes:response.data.data})
            })
            .catch(error=>{
            console.log("ALL Categories ERROR")
              console.log(error.response)
            })
          }
  

         onChange = (e) => {
            ///console.log(Array.from(e.target.files))
            this.setState({file:e.target.files});
           // console.log(e.target.files[0])
        }
        
         componentDidMount()
         {
             this.getCategories()
             this.getSize()
            // console.log("ANWEr")
             //console.log(this.props.currentUser)
            
             //this.props.location.state.data
             //console.log(this.props.location.state.data)
         }
    //submit form
    handleSubmit = (e) => {
        e.preventDefault();
       
        this.props.form.validateFieldsAndScroll(['offerPrice','offerDescription','offerRatio'],(err, values) => {
          if (!err) {
            //console.log('Received values of form: ', values);
            this.offerProduct(values.offerPrice,values.offerDescription,values.offerRatio)
        }
        });
      
      }

      editHandleSubmit = (e) => {
        e.preventDefault();
        const {product} = this.state
        this.props.form.validateFieldsAndScroll( ['name','price','quantity','category','description','size','company'],(err, values) => {
          if (!err) {
           
            var form = new FormData();
            if(this.state.file){
              for(var i=0 ; i<= this.state.file.length-1 ; i++)
          {
              form.append(`img`,this.state.file[i] )     
           // console.log("ooo");
          } 
          }  
            form.append('name', values.name);
            form.append('price', values.price);
            form.append('description', values.description);
            form.append('quantity', values.quantity);
            form.append('company', values.company);
            form.append('category', values.category.key);
            form.append('size', values.size.key);
            if(this.state.Colors.length >0){
              form.append('color', JSON.stringify(this.state.Colors));
            }
            if(this.state.sizesCustom.length >0){
              form.append('sizes', JSON.stringify(this.state.sizesCustom));
            }
            /*for(var i=0;i<values.size.length;i=i+1){
              form.append('size', values.size[i]);

            }

*/
            let l = message.loading('Wait..', 2.5)
            axios.put(`${BASE_END_POINT}products/${this.state.product.id}`,form,{
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.props.currentUser.token}`
              },
            })
            .then(response=>{
                l.then(() => message.success('Update Product', 2.5));
                this.setState({ editModel:false });
                this.props.history.goBack()
            })
            .catch(error=>{
               // console.log(error.response)
                l.then(() => message.error('Error', 2.5))
            })
        }
        });
        
      }
    //end submit form


    showModal = () => {
      this.setState({
        visible: true,
      });
    }
  
    handleOk = (e) => {
      //console.log(e);
      this.setState({
        visible: false,
      });
    }
  
    handleCancel = (e) => {
     // console.log(e);
      this.setState({
        visible: false,
      });
    }

    deleteProduct = () => {
        let l = message.loading('Wait..', 2.5)
        axios.delete(`${BASE_END_POINT}/products/${this.state.product.id}`,{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
        .then(response=>{
            l.then(() => message.success('Product Deleted', 2.5))
            this.props.history.goBack()

        })
        .catch(error=>{
         //   console.log(error.response)
            l.then(() => message.error('Error', 2.5))
        })
     }

    offerProduct = (offer,des,offerRatio) => {
      
       let l = message.loading('Wait..', 2.5)
        axios.put(`${BASE_END_POINT}products/${this.state.product.id}/offer`,JSON.stringify({
            offerPrice:offer,
            offerDescription:des,
            offerRatio:offerRatio
          }),{
         headers: {
           'Content-Type': 'application/json',
           'Authorization': `Bearer ${this.props.currentUser.token}`
         },
       })
        .then(response=>{
           //  console.log('done')
             l.then(() => message.success('Product Offerd', 2.5))
             this.setState({ visible:false });
             this.props.history.goBack()
        })
        .catch(error=>{
        // console.log('Error')
        // console.log(error.response)
         l.then(() => message.error('Error', 2.5))
        })
    }

    unofferProduct = () => {
      
        let l = message.loading('Wait..', 2.5)
         axios.delete(`${BASE_END_POINT}products/${this.state.product.id}/offer`,{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
         .then(response=>{
          //    console.log('done')
              l.then(() => message.success('Product Unofferd', 2.5))
              this.props.history.goBack()
         })
         .catch(error=>{
          //console.log('Error')
          //console.log(error.response)
          l.then(() => message.error('Error', 2.5))
         })
     }

     activeProduct = () => {
      
        let l = message.loading('Wait..', 2.5)
         axios.put(`${BASE_END_POINT}products/${this.state.product.id}/active`,{},{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
         .then(response=>{
            //  console.log('done')
              
              l.then(() => message.success('Product Active', 2.5))
              this.props.history.goBack()
         })
         .catch(error=>{
          //console.log('Error')
          //console.log(error.response)
          l.then(() => message.error('Error', 2.5))
         })
     }

     disactiveProduct = () => {
      
        let l = message.loading('Wait..', 2.5)
         axios.put(`${BASE_END_POINT}products/${this.state.product.id}/dis-active`,{},{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
         .then(response=>{
            //  console.log('done')
              l.then(() => message.success('Product Disactive', 2.5))
              this.props.history.goBack()
         })
         .catch(error=>{
          //console.log('Error')
          //console.log(error.response)
          l.then(() => message.error('Error', 2.5))
         })
     }

     topProduct = () => {
      
        let l = message.loading('Wait..', 2.5)
         axios.put(`${BASE_END_POINT}products/${this.state.product.id}/top`,{},{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
         .then(response=>{
            //  console.log('done')
              l.then(() => message.success('Product become Top', 2.5))
              this.props.history.goBack()
         })
         .catch(error=>{
          //console.log('Error')
          //console.log(error.response)
          l.then(() => message.error('Error', 2.5))
         })
     }

     lowProduct = () => {
      
        let l = message.loading('Wait..', 2.5)
         axios.put(`${BASE_END_POINT}products/${this.state.product.id}/low`,{},{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
         .then(response=>{
            //  console.log('done')
              l.then(() => message.success('Product become Low', 2.5))
              this.props.history.goBack()
         })
         .catch(error=>{
          //console.log('Error')
          //console.log(error.response)
          l.then(() => message.error('Error', 2.5))
         })
     }
  
    render() {
        const { getFieldDecorator } = this.props.form;
         //select
         const Option = Select.Option;
         const {product} = this.state

         function handleChange(value) {
            console.log(value); 
         }
         //end select
         const {select} = this.props;
         const { Colors, colorInput, colorValue,sizesCustom, sizesInput, sizesValue } = this.state;
         const colorsChild = Colors.map(this.forColorMap);
         const sizesChild = sizesCustom.map(this.forsizesMap);
      return (
          
        <div>
         <AppMenu height={'150%'} goTo={this.props.history}></AppMenu>
              <Nav></Nav>
        <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}} className='menu'>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#25272e'}}>
                    <h2 class="center-align" style={{color:'#fff'}}>{allStrings.ProductInfo}</h2>
                </div>
                <div className='row' style={{marginBottom:"0px"}}>
                <Carousel options={{duration: 15}} images={product.img} />
                </div>
                <div class="row" style={{marginTop:"0px"}}>
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={product.name}>
                            </input>
                            <label for="name" class="active">{allStrings.name}</label>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="description" type="text" class="validate" disabled disabled value={product.description}></input>
                            <label for="description" class="active">{allStrings.description}</label>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="category" type="text" class="validate" disabled disabled value={product.category.categoryname}>
                            </input>
                            <label for="category" class="active">{allStrings.category}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="company" type="text" class="validate" disabled disabled value={product.company}></input>
                            <label for="company" class="active">{allStrings.company}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="cashPrice" type="text" class="validate" disabled value={product.price}>
                            </input>
                            <label for="cashPrice" class="active">{allStrings.price}</label>
                            </div>
                           
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="hasOffer" type="text" class="validate" disabled value={""+product.hasOffer}>
                            </input>
                            <label for="hasOffer" class="active">{allStrings.hasoffer}</label>
                            </div> 
                            <div class="input-field col s6">
                            <input id="offer" type="text" class="validate" disabled value={product.offerPrice}></input>
                            <label for="offer" class="active">{allStrings.offerPrice}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="description" type="text" class="validate" disabled disabled value={product.offerRatio}></input>
                            <label for="description" class="active">{allStrings.offerRatio}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="offerDescription" type="text" class="validate" disabled value={product.offerDescription}></input>
                            <label for="offerDescription" class="active">{allStrings.offerdescription}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="description" type="text" class="validate" disabled disabled value={product.sallCount}></input>
                            <label for="description" class="active">{allStrings.saleCount}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="offerDescription" type="text" class="validate" disabled value={product.ratePrecent}></input>
                            <label for="offerDescription" class="active">{allStrings.rate}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="category" type="text" class="validate" disabled disabled value={product.size.size}>
                            </input>
                            <label for="category" class="active">{allStrings.EnglishSizeName}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="category" type="text" class="validate" disabled disabled value={product.size.arabicSize}>
                            </input>
                            <label for="category" class="active">{allStrings.arabicSizeName}</label>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="category" type="text" class="validate" disabled disabled value={product.color}>
                            </input>
                            <label for="category" class="active">{allStrings.color}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="category" type="text" class="validate" disabled disabled value={product.sizes}>
                            </input>
                            <label for="category" class="active">{allStrings.sizes}</label>
                            </div>
                            
                        </div>
                        {/*
                        <div className='dash-table'>
                            <h5>{allStrings.sizes} : </h5>
                            <div className='row'>
                                <div className="col s6 m6 l6 dashboard-table">
                                    <Table>
                                        <thead>
                                            <tr>
                                            <th data-field="id">{allStrings.id}</th>
                                            <th data-field="name">{allStrings.EnglishSizeName}</th>
                                            <th data-field="arabic">{allStrings.arabicSizeName}</th>
                                            
                                            </tr>
                                        </thead> 

                                        <tbody>
                                        {product.size.map(val=>(
                                            <tr>
                                                <td  onClick={()=>{this.props.history.push('/SizeInfo',{data:val}) }} >{val.id}</td>
                                                <td  onClick={()=>{this.props.history.push('/SizeInfo',{data:val}) }} >{val.size}</td>                                 
                                                <td  onClick={()=>{this.props.history.push('/SizeInfo',{data:val}) }} >{val.arabicSize}</td>
                                              
                                            </tr>
                                        ))}

                                        </tbody>
                                    </Table>
                                </div>
                            </div>
                        </div> 
                        */}
                        
                        

                            <a class="waves-effect waves-light btn btn-large delete " style={{width: '200px'}} onClick={this.deleteProduct}><i class="material-icons left spcial">delete</i>{allStrings.remove}</a>
                            <a class="waves-effect waves-light btn btn-large delete" style={{width: '200px'}} onClick={this.unofferProduct}><i class="material-icons left spcial">clear</i>{allStrings.removeoffer}</a>
                            <a class="waves-effect waves-light btn btn-large edit" style={{width: '200px'}} onClick={this.showModal}><i class="material-icons left spcial">assistant</i>{allStrings.addoffer}</a>
                            <a class="waves-effect waves-light btn btn-large edit" style={{width: '200px'}} onClick={()=>this.setState({editModel:true})}><i class="material-icons left spcial">edit</i>{allStrings.edit}</a>
                           
                         
                            <a class="waves-effect waves-light btn btn-large" style={{width: '200px',backgroundColor:'#2bbbad'}} onClick={this.activeProduct}><i class="material-icons left spcial">done</i>{allStrings.active}</a>
                           <a class="waves-effect waves-light btn btn-large" style={{width: '200px',backgroundColor:'#2bbbad'}} onClick={this.topProduct}><i class="material-icons left spcial">expand_less</i>{allStrings.top}</a>
                            
                           <a class="waves-effect waves-light btn btn-large warning" style={{width: '200px'}} onClick={this.lowProduct}><i class="material-icons left spcial">expand_more</i>{allStrings.low}</a>
                            <a class="waves-effect waves-light btn btn-large warning" style={{width: '200px'}} onClick={this.disactiveProduct}><i class="material-icons left spcial">close</i>{allStrings.disactive}</a>
                           

                            <div>
                                <Modal
                                title={allStrings.addoffer}
                                visible={this.state.visible}
                                onOk={this.handleSubmit}
                                onCancel={this.handleCancel}
                                okText={allStrings.ok}
                                cancelText={allStrings.cancel}
                                >
                              
                                <Form onSubmit={this.handleSubmit} className="login-form">
                                    <Form.Item>
                                    {getFieldDecorator('offerPrice', {
                                        rules: [{ required: true, message: 'Please enter offer price' }],
                                        
                                    })(
                                        <Input placeholder={allStrings.offer}  />
                                    )}
                                    </Form.Item>
                                    <Form.Item>
                                    {getFieldDecorator('offerRatio', {
                                        rules: [{ required: true, message: 'Please enter offer ratio' }],
                                        
                                    })(
                                        <Input placeholder={allStrings.offerRatio}  />
                                    )}
                                    </Form.Item>

                                    <Form.Item>
                                    {getFieldDecorator('offerDescription', {
                                        rules: [{ required: true, message: 'Please enter offer description' }],
                                        
                                    })(
                                        <Input placeholder={allStrings.offerdescription}  />
                                    )}
                                    </Form.Item>

                                    

                                    </Form>
                                </Modal>

                                <Modal
                                    title={allStrings.edit}
                                    visible={this.state.editModel}
                                    onOk={this.editHandleSubmit}
                                    okText={allStrings.ok}
                                cancelText={allStrings.cancel}
                                    onCancel={() => this.setState({editModel:false})}
                                >
                                <input type="file" multiple onChange= {this.onChange}></input>
                                    <Form onSubmit={this.editHandleSubmit} className="login-form">
                                    <Form.Item>
                                    {getFieldDecorator('name', {
                                        rules: [{ required: true, message: 'Please enter product anme' }],
                                        initialValue: product.name
                                    })(
                                        <Input placeholder={allStrings.name}/>
                                    )}
                                    </Form.Item>
                                    <Form.Item>
                                    {getFieldDecorator('price', {
                                        rules: [{ required: true, message: 'Please enter product price' }],
                                        initialValue: product.price
                                    })(
                                        <Input placeholder={allStrings.price}/>
                                    )}
                                    </Form.Item>
                                    <Form.Item>
                                    {getFieldDecorator('description', {
                                        rules: [{ required: true, message: 'Please enter description' }],
                                        initialValue: product.description
                                    })(
                                        <Input placeholder={allStrings.description}/>
                                    )}
                                    </Form.Item>
                                    <Form.Item>
                                    {getFieldDecorator('company', {
                                        rules: [{ required: true, message: 'Please enter company' }],
                                        initialValue: product.company
                                    })(
                                        <Input placeholder={allStrings.company}/>
                                    )}
                                    </Form.Item>
                                    <Form.Item>
                                    {getFieldDecorator('quantity', {
                                        rules: [{ required: true, message: 'Please enter count' }],
                                        initialValue: product.quantity
                                    })(
                                        <Input placeholder={allStrings.count} type='number'/>
                                    )}
                                    </Form.Item>
                                   
                                    <Form.Item>
                                    {getFieldDecorator('category', {
                                        rules: [{ required: true, message: 'Please enter category' }],
                                    })(
                                        <Select labelInValue  
                                        placeholder={allStrings.category}
                                        style={{ width: '100%'}} >
                                        {this.state.categories.map(
                                            val=><Option value={val.id}>{val.categoryname}</Option>
                                        )}
                                        </Select>
                                    )}
                                    </Form.Item>
                                    <Form.Item>
                                    {getFieldDecorator('size', {
                                        rules: [{ required: true, message: 'Please enter size' }],
                                    })(
                                        <Select labelInValue  
                                        placeholder={allStrings.size}
                                        style={{ width: '100%'}} >
                                        {this.state.sizes.map(
                                            val=><Option value={val.id}>{val.size}</Option>
                                        )}
                                        </Select>
                                    )}
                                    </Form.Item>
                                    <div>
                        <div style={{ marginBottom: 16 }}>
                            {colorsChild}
                        </div>
                        {colorInput && (
                          <Input
                            ref={this.saveInputColorRef}
                            type="text"
                            style={{ width: '100%' }}
                            value={colorValue}
                            onChange={this.handelInputColorChange}
                            onBlur={this.handelInputColorConfirm}
                            onPressEnter={this.handelInputColorConfirm}
                          />
                        )}
                        {!colorInput && (
                          <Tag onClick={this.showColorInput} className="site-tag-plus">
                            + {allStrings.addColor}
                          </Tag>
                        )}
                      </div>
                      <div>
                        <div style={{ marginBottom: 16 }}>
                            {sizesChild}
                        </div>
                        {sizesInput && (
                          <Input
                            ref={this.saveInputsizesRef}
                            type="text"
                            style={{ width: '100%' }}
                            value={sizesValue}
                            onChange={this.handelInputsizesChange}
                            onBlur={this.handelInputsizesConfirm}
                            onPressEnter={this.handelInputsizesConfirm}
                          />
                        )}
                        {!sizesInput && (
                          <Tag onClick={this.showsizesInput} className="site-tag-plus">
                            + {allStrings.addSize}
                          </Tag>
                        )}
                      </div>
                                    {/*}
                                    <Form.Item>
                                    {getFieldDecorator('size', {
                                        rules: [{ required: true, message: 'Please enter size' }],
                                    })(
                                      <Select 
                                      mode="multiple"
                                      style={{ width: '100%' }}
                                      placeholder="select size"
                                      onChange={handleChange}
                                      optionLabelProp="label " 
                                    >
                                      {this.state.sizes.map(
                                            val=><Option value={val.id} label={val.size}>{val.size} </Option>
                                        )}
                                      
                                    </Select>
                                    )}
                                    </Form.Item> */}
                                    </Form>
                                </Modal>
                        
                            </div>
                        </form>
                        
                    </div>
            </div>
        </div>
        </div>
        <Footer></Footer>
        </div>
    </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,


  })
  
  const mapDispatchToProps = {
  }

export default connect(mapToStateProps,mapDispatchToProps) (ProductInfo = Form.create({ name: 'normal_login', })(ProductInfo));

import React, { Component } from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';

import './dashboard.css';
import {Icon,Table} from 'react-materialize'
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import {NavLink} from 'react-router-dom';
import { notification,Skeleton,Checkbox,message} from 'antd';
import 'antd/dist/antd.css';
import firebase from 'firebase';
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import {withRouter} from 'react-router-dom'


class Dashboard extends Component {
   
  state = {
    users:[],
    counts:[],
    actions:[],
    loading:true,
    loading2:true,
    checked:false,
   
}

constructor(props){
    super(props)
    if(this.props.isRTL){
      allStrings.setLanguage('ar')
    }else{
      allStrings.setLanguage('en')
    }

  }
  
 
    componentDidMount(){
       // console.log(this.props.currentUser)
        console.log("push   ",this.props.history)
        this.getCounts()
        this.getOption()
        this.getLastAction()
            this.getLastUsers()
            this.reciveNotification()
    }
      
    reciveNotification = async () => {
        try{
          const messaging = firebase.messaging();
          await messaging.requestPermission();
           await messaging.onMessage(msg=>{
              notification.open({
                  message: msg.notification.title,
                  description: msg.notification.body,
                  icon:  <Icon>group</Icon>,
                });          
         //     console.log('user token: ', msg)
           });
          
        }catch (error) {
          //console.error(error);
        }
    }
   
       
       //submit form
       //PENDING
       getCounts= () => {
         axios.get(`${BASE_END_POINT}admin/count`,{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
         .then(response=>{
           console.log("ALL counts")
           console.log(response.data)
           this.setState({counts:response.data,loading:false})
         })
         .catch(error=>{
           console.log("ALL counts ERROR")
           console.log(error.response)
         })
       }
       getLastUsers= () => {
        axios.get(`${BASE_END_POINT}admin/users`,{
           headers: {
             'Content-Type': 'application/json',
             'Authorization': `Bearer ${this.props.currentUser.token}`
           },
         })
        .then(response=>{
          //console.log("ALL last users")
          //console.log(response.data)
          this.setState({users:response.data,loading2:false})
        })
        .catch(error=>{
          //console.log("ALL last users ERROR")
          //console.log(error.response)
        })
      }
      getLastAction= () => {
        axios.get(`${BASE_END_POINT}admin/actions`,{
           headers: {
             'Content-Type': 'application/json',
             'Authorization': `Bearer ${this.props.currentUser.token}`
           },
         })
        .then(response=>{
          //console.log("ALL last actions")
          //console.log(response.data)
          this.setState({actions:response.data,loading2:false})
        })
        .catch(error=>{
          //console.log("ALL last orders ERROR")
         // console.log(error.response)
        })
      }
      getOption= () => {
        axios.get(`${BASE_END_POINT}option`,{
           headers: {
             'Content-Type': 'application/json',
             'Authorization': `Bearer ${this.props.currentUser.token}`
           },
         })
        .then(response=>{
          console.log(response.data[0].enable)
          this.setState({checked:response.data[0].enable,loading:false})
        })
        .catch(error=>{
          console.log("ALL counts ERROR")
          console.log(error.response)
        })
      }
      option = (check) => {
        let uri ='';
        if(check){
         uri = `${BASE_END_POINT}/option/1/enable`
        }else{
         uri = `${BASE_END_POINT}/option/1/disable`
        }
       let l = message.loading(allStrings.wait, 2.5)
        axios.put(uri,{},{
         headers: {
           'Content-Type': 'application/json',
           'Authorization': `Bearer ${this.props.currentUser.token}`
         },
       })
        .then(response=>{
             console.log('done')
             if(check){
                 
                 l.then(() => message.success(allStrings.on, 2.5))
                 
             }else{
             
                l.then(() => message.success(allStrings.off, 2.5))
             }
        })
        .catch(error=>{
         console.log('Error')
         console.log(error.response)
         l.then(() => message.error('Error', 2.5))
        })
    }
        
    onChange = e => {
      console.log('checked = ', e.target.checked);
      this.setState({
        checked: e.target.checked,
      });
      if(e.target.checked === true){
        this.option(true)
      } else{
        this.option(false)
      }
    };
  render() {
      const {counts} = this.state;
      const {users} = this.state;
      const {actions} = this.state;
      const loadingView = 
       <Skeleton  active/> 

       const{select} = this.props
       //alert(''+window.innerHeight)
    return (
      <div >
        <AppMenu height={'200%'} goTo={this.props.history}></AppMenu>
        <Nav></Nav>
        
        <div style={{marginRight:!select?'20.2%':'5.5%'}} className='menu'>
          <div className='content' >
       
              <div className="row">
                  <div className="col s12 m6 xl4 l6 count1">
                  <NavLink to='/users'>
                  <div className="icons">
                      <Icon>group</Icon>
                  </div>
                  <div className='info' >
                      <p>{allStrings.user}</p>
                      <span>{this.state.loading?loadingView:counts.users}</span>
                  </div>
                  </NavLink>
                  </div>          
                 

                  <div className="col s12 m6 xl4 l6 count2">
                  <NavLink to='/announcement'>
                      <div className="icons">
                      <Icon>airplay</Icon> 
                      </div>
                      <div className='info'>
                          <p>{allStrings.ads}</p>
                          <span>{this.state.loading?loadingView:""+counts.ads}</span>
                      </div>
                  </NavLink>
                  </div>


                  <div className="col s12 m6 xl4 l6 count3">
                  <NavLink to='/category'>
                      <div className="icons">
                      <Icon>turned_in_not</Icon> 
                      </div>
                      <div className='info'>
                          <p>{allStrings.category}</p>
                          <span>{this.state.loading?loadingView:counts.category}</span>
                      </div>
                  </NavLink>
                  </div> 
                
              </div>
              
              <div className="row">
                  <div className="col s12 m6 xl4 l6 count1">
                  <NavLink to='/Products'>
                  <div className="icons">
                  <Icon>shopping_basket</Icon>
                  </div>
                  <div className='info'>
                      <p>{allStrings.product}</p>
                      <span>{this.state.loading?loadingView:""+counts.products}</span>
                  </div>
                  </NavLink>
                  </div>
                 
                  <div className="col s12 m6 xl4 l6 count2">
                  <NavLink to='/AcceptedOrders'>
                      <div className="icons">
                      <Icon>shopping_cart</Icon>
                      </div>
                      <div className='info'>
                          <p>{allStrings.acceptOrders}</p>
                          <span>{this.state.loading?loadingView:""+counts.accepted}</span>
                      </div>
                  </NavLink>
                  </div>
                  
                  <div className="col s12 m6 xl4 l6 count3">
                  <NavLink to='/PendingOrders'>

                  <div className="icons">
                  <Icon>add_shopping_cart</Icon>
                  </div>
                  <div className='info'>
                      <p>{allStrings.pendingOrder}</p>
                      <span>{this.state.loading?loadingView:""+counts.pending}</span>
                  </div>
                  </NavLink>
                  </div>
                                
              </div>

              <div className="row">
                  <div className="col s12 m6 xl4 l6 count1">
                  <NavLink to='/RefusedOrders'>
                  <div className="icons">
                  <Icon>close</Icon>
                  </div>
                  <div className='info'>
                      <p>{allStrings.refusedOrders}</p>
                      <span>{this.state.loading?loadingView:""+counts.refused}</span>
                  </div>
                  </NavLink>
                  </div>
                 
                  <div className="col s12 m6 xl4 l6 count2">
                  <NavLink to='/DeliverdOrders'>
                      <div className="icons">
                      <Icon>check</Icon>
                      </div>
                      <div className='info'>
                          <p>{allStrings.deliverdOrder}</p>
                          <span>{this.state.loading?loadingView:""+counts.delivered}</span>
                      </div>
                  </NavLink>
                  </div>
                  
                  <div className="col s12 m6 xl4 l6 count2">
                    <NavLink to='/cities'>
                    <div className="icons">
                    <Icon>assistant_photo</Icon>
                    </div>
                    <div className='info'>
                        <p>{allStrings.cities}</p>
                        <span>{this.state.loading?loadingView:counts.cities}</span>
                    </div>
                    </NavLink>
                  </div>
                                
              </div>


              <div className="row">
                  <div className="col s12 m6 xl4 l6 count1">
                  <NavLink to='/Contactus'>

                  <div className="icons">
                   <Icon>mail_outline</Icon>
                  </div>
                  <div className='info'>
                      <p>{allStrings.messages}</p>
                      <span>{this.state.loading?loadingView:""+counts.messages}</span>
                  </div>
                  </NavLink>
                  </div>
                 
                  <div className="col s12 m6 xl4 l6 count2">
                    <div className="icons">
                    <Icon>local_atm</Icon>
                    </div>
                    <div className='info'>
                        <p>{allStrings.totalPerDay}</p>
                        <span>{this.state.loading?loadingView:""+counts.totalPerDay}</span>
                    </div>
                  </div>
                 
                  <div className="col s12 m6 xl4 l6 count3">
                      <div className="icons">
                      <Icon>attach_money</Icon>
                      </div>
                      <div className='info'>
                          <p>{allStrings.totalSales}</p>
                          <span>{this.state.loading?loadingView:counts.totalSales}</span>
                      </div>
                  </div> 
                                   
              </div>
            


          </div>
          </div>
      </div>
    );
  }
}

const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
    
  }

export default withRouter( connect(mapToStateProps,mapDispatchToProps) (Dashboard) );

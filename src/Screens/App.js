import React, { Component } from 'react';
import Dashboard from './dashboard/dashboard';
import Users from './users/users';
import Category from './category/category';
import City from './city/city';
//import Usage from './usage/usage';
import About from './about/about';
import Terms from './terms/terms';
import Privacy from './privacy/privacy';

import Report from './report/report';
import Login from './login/login';
import Admin from './admin/admin'


import  AnoncementInfo from './anoncement info/anoncement info'

import Anoncement from './anoncement/anoncement'
import Contactus from './contactus/contactus'
import AdminInfo from './admin info/admin info'
import UserInfo from './user info/user info'
import Splash from './splash/splash'
import CategoryInfo from './category info/category info'
import CountryInfo from './city info/city info'
import { Route, BrowserRouter, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'
import {store,persistor } from '../store';
import { PersistGate } from 'redux-persist/integration/react'
import Conditions from './conditions/conditions'
import CityInfo from './city info/city info'
import AreaInfo from './area info/area info'
import Size from './size/size'
import SizeInfo from './size info/size info'
import Products from './product/product'
import ProductInfo from './product info/product info'
import Order from './order/order'
import AcceptedOrder from './accept order/accept order'
import RefusedOrder from './refused order/refused order'
import DeliverdOrder from './deliverd order/deliverd order'
import OrderInfo from './order info/order info'
import TopSellerProduct from './topSellerProduct/topSellerProduct'
import Logo from './logo/logo'

class App extends Component {
  componentDidMount(){
   // askForPermissioToReceiveNotifications()
  }
  //
  render() {
    return (
      <Provider store={store}>
         <PersistGate loading={null} persistor={persistor}>
      <BrowserRouter>
        <div className="App">
          <Switch>
            <Route path='/Dashboard' component={Dashboard}/>
            <Route  path='/Login' component={Login}/>
           
            <Route  path='/CityInfo' component={CityInfo}/>
           
            <Route  path='/Conditions' component={Conditions}/>
            <Route path='/Contactus' component={Contactus}/>
            <Route path='/AnoncementInfo' component={AnoncementInfo}/>
            
            <Route exact path='/' component={Splash}/>
            <Route path='/users' component={Users} />
            <Route path='/terms' component={Terms} />
            <Route path='/Admins' component={Admin} />
            <Route path='/category' component={Category} />
           
            <Route path='/reports' component={Report} />
            <Route path='/AdminInfo' component={AdminInfo} />
            <Route path='/UserInfo' component={UserInfo} />
            <Route path='/CategoryInfo' component={CategoryInfo} />
            <Route path='/announcement' component={Anoncement} />
            <Route path='/cities' component={City} />
            <Route path='/AreaInfo' component={AreaInfo} />
            <Route path='/sizes' component={Size} />
            <Route path='/SizeInfo' component={SizeInfo} />
            <Route path='/about' component={About} />
            {/*<Route path='/usage' component={Usage} /> */}
            <Route path='/privacy' component={Privacy} />
            <Route path='/Products' component={Products} />
            <Route path='/ProductInfo' component={ProductInfo} />
            <Route path='/PendingOrders' component={Order} />
            <Route path='/AcceptedOrders' component={AcceptedOrder} />
            <Route path='/RefusedOrders' component={RefusedOrder} />
            <Route path='/DeliverdOrders' component={DeliverdOrder} />
            <Route path='/OrderInfo' component={OrderInfo} />
            <Route path='/topSellerProduct' component={TopSellerProduct}/>
            <Route path='/website' component={Logo}/>

          </Switch>
        </div>
      </BrowserRouter>
      </PersistGate>
      </Provider>
    );
  }
}

export default App;

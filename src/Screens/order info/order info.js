import React from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import './order info.css';
import "antd/dist/antd.css";
 import { Modal,Form ,Select ,DatePicker,message,Input} from 'antd';
 import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import {Table} from 'react-materialize'
import {withRouter} from 'react-router-dom'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'


class OrderInfo extends React.Component {
    state = {
        visible: false,
        order:this.props.location.state.data,
      
    }

    constructor(props){
        super(props)
        if(this.props.isRTL){
          allStrings.setLanguage('ar')
        }else{
          allStrings.setLanguage('en')
        }

        //status: "ACCEPTED"
       // console.log("Order info  ",this.props.location.state.data)
      }

    componentDidMount(){
        //console.log(this.props.currentUser)
        //console.log("pop   ",this.props.history)
    }
    deleteOrder = () => {
        let l = message.loading('Wait..', 2.5)
        axios.delete(`${BASE_END_POINT}/orders/${this.state.order.id}`,{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
        .then(response=>{
            l.then(() => message.success('order Deleted', 2.5))
            /*if(this.order.status === "PENDING"){
                this.props.history.push('/pendingOrder');
            }*/
            this.props.history.goBack()
           

        })
        .catch(error=>{
           // console.log(error.response)
            l.then(() => message.error('Error', 2.5))
        })
     }
    //submit form
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
           // console.log('Received values of form: ', values);
            const data = {
                status:values.status.key
            }
         
            if(values.reason){
                data.reason = values.reason
            }

           // console.log(data)
            
            let l = message.loading('Wait..', 2.5)
          axios.put(`${BASE_END_POINT}orders/${this.state.order.id}/status`,JSON.stringify(data),{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
          .then(response=>{
              l.then(() => message.success('Order Updated', 2.5))
              this.setState({ visible:false });
              this.props.history.goBack()
          })
          .catch(error=>{
             // console.log(error.response)
              l.then(() => message.error('Error', 2.5))
          })
          
          }
        });
        
      }
    //end submit form
  

    showModal = () => {
      this.setState({
        visible: true,
      });
    }
  
    handleOk = (e) => {
      //console.log(e);
      this.setState({
        visible: false,
      });
    }
  
    handleCancel = (e) => {
      //console.log(e);
      this.setState({
        visible: false,
      });
    }
  
    render() {
        const { getFieldDecorator } = this.props.form;
         //select
         const Option = Select.Option;
         const {order} = this.state
         const {select} = this.props;
         
         //end select
       
      return (
        <div>
        <AppMenu height={'150%'} goTo={this.props.history}></AppMenu>
         <Nav></Nav>
         <div style={{marginRight:!select?'20.2%':'5.5%'}} className='menu'>
         <div className='login'>
       <div class="row">
           <div class="col m2">
               <div class='title' style={{backgroundColor:'#25272e'}} >
                   <h2 class="center-align" style={{color:'#fff'}}>{allStrings.orderInfo}</h2>
               </div>
               <div class="row">
                   <form class="col s12">


                       <div className='dash-table'>
                           <h5>{allStrings.client} :</h5>
                           <div className='row'>
                               <div className="col s6 m6 l6 dashboard-table">
                                   <Table>
                                       <thead>
                                           <tr>
                                           <th data-field="id">{allStrings.id}</th>
                                           <th data-field="name">{allStrings.name}</th>
                                           <th data-field="email">{allStrings.email}</th>
                                           <th data-field="mobile">{allStrings.phone}</th>
                                           <th data-field="address">{allStrings.address}</th>
                                           </tr>
                                       </thead>

                                       <tbody>
                                           <tr onClick={()=>{
                                               this.props.history.push('/UserInfo',{data:this.state.order.client})
                                               }}>
                                               <td>{order.client.id}</td>
                                               <td>{order.client.firstname +" "+  order.client.lastname}</td>
                                               <td>{order.client.email}</td>
                                               <td>{order.client.phone}</td>
                                               <td>{order.client.address}</td>
                                           </tr>
                                       </tbody>
                                   </Table>
                               </div>
                           </div>
                       </div>

                       <div className='dash-table'>
                           <h5>{allStrings.product} :</h5>
                           <div className='row'>
                               <div className="col s6 m6 l6 dashboard-table">
                                   <Table>
                                       <thead>
                                           <tr>
                                           <th data-field="id">{allStrings.id}</th>
                                           <th data-field="name">{allStrings.name}</th>
                                           <th data-field="company">{allStrings.company}</th>
                                           <th data-field="price">{allStrings.price}</th>
                                           <th data-field="count">{allStrings.hasoffer}</th>
                                           <th data-field="count">{allStrings.offerPrice}</th>
                                           <th data-field="count">{allStrings.count}</th>
                                           <th data-field="count">{allStrings.color}</th>
                                           <th data-field="count">{allStrings.sizes}</th>
                                           </tr>
                                       </thead>

                                       <tbody>
                                           {order.productOrders.map(val=>(
                                           <tr onClick={()=>{
                                               this.props.history.push('/ProductInfo',{data:val.product})
                                               
                                           }}
                                           >
                                           <td>{val.product.id}</td>
                                           <td>{val.product.name}</td>
                                           <td>{val.product.company}</td>
                                           <td>{val.product.price}</td>
                                           <td>{""+val.product.hasOffer}</td>
                                           <td>{val.product.offerPrice}</td>
                                           <td>{val.color}</td>
                                           <td>{val.sizes}</td>
                                           </tr>
                                           ))}
                                          
                                       </tbody>
                                   </Table>
                               </div>
                           </div>
                       </div>
                       <div class="row">
                           <div class="input-field col s12">
                           <input id="Status" type="text" class="validate" disabled disabled value={order.status}></input>
                           <label for="Status" class="active">{allStrings.status}</label>
                           </div>
                       </div>
       
                       <div class="row">
                           <div class="input-field col s6">
                           <input id="total" type="text" class="validate" disabled value={order.total}></input>
                           <label for="total" class="active">{allStrings.total}</label>
                           </div>
                      
                           <div class="input-field col s6">
                           <input id="total" type="text" class="validate" disabled value={order.delivaryCost}></input>
                           <label for="total" class="active">{allStrings.delivaryCost}</label>
                           </div>
                       </div>
                       <div class="row">
                           <div class="input-field col s12">
                           <input id="total" type="text" class="validate" disabled value={order.finalTotal}></input>
                           <label for="total" class="active">{allStrings.finalTotal}</label>
                           </div>
                       </div>
                       <div class="row">
                           <div class="input-field col s12">
                           <input id="total" type="text" class="validate" disabled value={order.city.cityName}></input>
                           <label for="total" class="active">{allStrings.city}</label>
                           </div>
                       </div>
                       <div class="row">
                           <div class="input-field col s12">
                           <input id="total" type="text" class="validate" disabled value={order.area.areaName}></input>
                           <label for="total" class="active">{allStrings.area}</label>
                           </div>
                       </div>
                       <div class="row">
                           <div class="input-field col s12">
                           <input id="total" type="text" class="validate" disabled value={order.address}></input>
                           <label for="total" class="active">{allStrings.address}</label>
                           </div>
                       </div>
                       <div class="row">
                           <div class="input-field col s12">
                           <input id="total" type="text" class="validate" disabled value={order.reason}></input>
                           <label for="total" class="active">{allStrings.reason}</label>
                           </div>
                       </div>
                       <div class="row">
                           <div class="input-field col s12">
                           <input id="total" type="text" class="validate" disabled value={order.paymentSystem}></input>
                           <label for="total" class="active">{allStrings.paymentSystem}</label>
                           </div>
                       </div>

                       {order.status == 'PENDING'&&
                           <a class="waves-effect waves-light btn btn-large delete" onClick={this.deleteOrder}><i class="spcial material-icons left">delete</i>{allStrings.remove}</a>
                       }  
                           {
                             this.props.location.state.data.status!='DELIVERED'&&
                             <a class="waves-effect waves-light btn btn-large edit" onClick={this.showModal}><i class="material-icons left spcial">edit</i>{allStrings.changestatus}</a>

                           }
            
                           <div>
                               <Modal
                               title={allStrings.changestatus}
                               visible={this.state.visible}
                               onOk={this.handleSubmit}
                               onCancel={this.handleCancel}
                               okText={allStrings.ok}
                               cancelText={allStrings.cancel}
                               >
                               <Form onSubmit={this.handleSubmit} className="login-form">
                   
                                   <Form.Item>
                                       {getFieldDecorator('status', {
                                           rules: [{ required: true, message: 'Please enter status' }],
                                       })(
                                           <Select labelInValue  
                                           placeholder={allStrings.status}
                                           style={{ width: '100%'}} >
                                               {
                                                  this.props.location.state.data.status==='PENDING'&&
                                                  <Option value="ACCEPTED">{allStrings.accepted}</Option>
                                               }
                                               
                                               {
                                                 this.props.location.state.data.status==='PENDING'&&
                                                 <Option value="REFUSED">{allStrings.refused}</Option>
                                               }
                                              
                                               {
                                                  this.props.location.state.data.status==='ACCEPTED'&&
                                                  <Option value="DELIVERED">{allStrings.deliverd}</Option>
                                               }
                                               
                                               
                                           </Select>
                                       )}
                                       </Form.Item>
                                       
                                       {order.status == 'PENDING'&&
                                       <Form.Item>
                                           {getFieldDecorator('reason', {
                                               rules: [{ required: false, message: 'Please enter description' }],
                                           })(
                                               <Input placeholder={allStrings.reason} />
                                           )}
                                           </Form.Item> 
                                       }
                               </Form>
                               </Modal>
                           </div>
                       </form>
                       
                   </div>
           </div>
       </div>
       </div>
       <Footer></Footer>
       </div>
   </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,


  })
  
  const mapDispatchToProps = {
  }

export default withRouter( connect(mapToStateProps,mapDispatchToProps) (OrderInfo = Form.create({ name: 'normal_login' })(OrderInfo)));



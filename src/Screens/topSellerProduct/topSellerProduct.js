import React from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import Tables from '../../components/table/table';
import './product.css';
import { Skeleton,Form, Icon, Input, Button, Popconfirm, message} from 'antd';

import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import {withRouter} from 'react-router-dom'
import  {allStrings} from '../../assets/strings'
class topSellerProduct extends React.Component {
    //submit form
    pagentationPage=0;
    counter=0;
    state = {
      selectedSizes:'',
      modal1Visible: false,
      confirmDelete: false,
        selectedproducts:null,
      products:[],
      loading:true,
      file: null,
      deletedRow:null,
      tablePage:0,
      companyName:'',
      inputValue:''
      }
//


      constructor(props){
        super(props)
        if(this.props.isRTL){
          allStrings.setLanguage('ar')
        }else{
          allStrings.setLanguage('en')
        }
        this.updateInput = this.updateInput.bind(this);

      }
     
     flag = 0;

     getproducts = (page,deleteRow) => {
      
       axios.get(`${BASE_END_POINT}products?page=1&limit={10}&topSeller=true`)
       .then(response=>{
         //console.log("ALL products")
         //console.log(response.data)
         if(deleteRow){
          this.setState({tablePage:0})
         }
         this.setState({products:deleteRow?response.data.data:[...this.state.products,...response.data.data],loading:false})
       })
       .catch(error=>{
        this.setState({loading:false})
         console.log("ALL products ERROR")
         console.log(error.response)
       })
     }
     getproductsfilter = (page,deleteRow,companyName) => {
      console.log(this.state.inputValue)
      axios.get(`${BASE_END_POINT}products/?company=${this.state.inputValue}&page=1&limit={10}&topSeller=true`)
      .then(response=>{
        //console.log("ALL products")
        //console.log(response.data)
        if(deleteRow){
         this.setState({tablePage:0})
        }
        this.setState({products:deleteRow?response.data.data:[...this.state.products,...response.data.data],loading:false})
        document.getElementById('searchCompany').value = ''
      })
      .catch(error=>{
       this.setState({loading:false})
        console.log("ALL products ERROR")
        console.log(error.response)
      })
    }

 
     componentDidMount(){
       this.getproducts(1)
     }
     OKBUTTON = (e) => {
      this.deleteproducts()
     }

     deleteproducts = () => {
       let l = message.loading('Wait..', 2.5)
       //console.log(this.state.selectedproducts);
       axios.delete(`${BASE_END_POINT}products/${this.state.selectedproducts}`,{
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${this.props.currentUser.token}`
        },
       })
       .then(response=>{
           l.then(() => message.success('products Deleted', 2.5));
           this.getproducts(1,true)
             this.flag = -1
           
       })
       .catch(error=>{
           //console.log(error.response)
           l.then(() => message.error('Error', 2.5))
       })
    }
    
  
      
        updateInput(e){
          this.setState({inputValue : e.target.value})
          }
     
    render() {
      
          let controls = (
            <Popconfirm
            title={allStrings.areusure}
            onConfirm={this.OKBUTTON}
            onCancel={this.fCANCELBUTTON}
            okText={allStrings.yes}
            cancelText={allStrings.cancel}
          >
             <Icon className='controller-icon' type="delete" />
          </Popconfirm>
         )

         let list =this.state.products.map((val,index)=>[
          val.id,""+val.name,""+val.price,
          ""+val.quantity,""+val.category.arabicname,""+val.company,
          ""+val.sallCount,""+val.available,controls
        ])

        const loadingView = [
          [<Skeleton  active/> ],
          [<Skeleton active/> ],
          [<Skeleton  active/> ],
          [<Skeleton active/> ],
          
         ]
const {select} = this.props;
      return (
          <div>
              <AppMenu  height={'140%'} goTo={this.props.history}></AppMenu>
              <Nav></Nav>
              
              <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}} className='menu'>
             <div className='filterCustom'>
              <div class="row">
                
                <div class="input-field col s12" style={{width: '90%', marginRight:'2%'}}>
                  <Input id="searchCompany" style={{width: '90%', marginRight:'2rem',marginLeft:'2rem'}} type='text' name='companyFilter' placeholder={allStrings.searchCompany} onChange={this.updateInput}></Input>
                  <Button style={{color: 'white', backgroundColor:'#3497fd', width: '85%', marginRight:'1rem',marginLeft:'1rem',height:'40px'}} onClick={() => this.getproductsfilter(1,`${this.state.inputValue}`)}>{allStrings.search}</Button>
                  <Icon className='controller-icon' type="undo" onClick={() => this.getproducts(1)} />
                </div>
              </div>
              </div>
              <Tables
               columns={this.state.loading?['loading...'] :[allStrings.id,allStrings.name,allStrings.price,allStrings.count,allStrings.category,allStrings.company,allStrings.saleCount,allStrings.active, allStrings.remove]} 
              title={allStrings.productTable}
              page={this.state.tablePage}
              onCellClick={(colData,cellMeta,)=>{
                //console.log('col index  '+cellMeta.colIndex)
                //console.log('row index   '+colData)
                if(cellMeta.colIndex!==7){
                  
                  //console.log(this.state.products[cellMeta.rowIndex])
                  this.props.history.push('/ProductInfo',{data:this.state.products[this.pagentationPage+cellMeta.rowIndex]})
                }else if(cellMeta.colIndex===7){
                  const id = list[this.pagentationPage +cellMeta.rowIndex][0];
                  this.setState({selectedproducts:id,deletedRow:cellMeta.rowIndex})
                    //console.log(id)
                  }
              }}
               onChangePage={(currentPage)=>{
                this.setState({tablePage:currentPage})
                if(currentPage>this.counter){
                  this.counter=currentPage;
                  this.pagentationPage=this.pagentationPage+10
                }else{ 
                 this.counter=currentPage;
                 this.pagentationPage=this.pagentationPage-10
                }
                //console.log(currentPage)
                if(currentPage%2!=0  && currentPage > this.flag){
                  let rem = currentPage %2;
                  let x = currentPage - rem;
                  console.log(rem)
                  console.log(x)
                  this.flag  = this.flag +1;
                  console.log("flage",this.flag)
                    let page = rem + this.flag;
                    console.log("page",page)
                    this.getproducts(page)
                    console.log("request",page)
                    console.log("flag",this.flag) 

                } 
                  
              }}
              arr={this.state.loading? loadingView:list}></Tables>
            <Footer></Footer>
             </div>
          </div>
      );
    }
  } 

  
  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,
  })
  
  const mapDispatchToProps = {

  }

  export default  withRouter(connect(mapToStateProps,mapDispatchToProps)(topSellerProduct = Form.create({ name: 'normal_login' })(topSellerProduct)));

import React from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Tables from '../../components/table/table';


import './report.css';
import { Skeleton, Icon,Popconfirm, message} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux'
import  {allStrings} from '../../assets/strings'


class Report extends React.Component {
  flag = -1;
 

  constructor(props){
    super(props)
    this.getAction(1,true)
    if(this.props.isRTL){
      allStrings.setLanguage('ar')
    }else{
      allStrings.setLanguage('en')
    }
  }

   
  pagentationPage=0;
  counter=0;
  state = {
    actions:[],
    confirmDelete: false,
    selectedAction:null,
    loading:true,
    tablePage:0,
}
  getAction= (page,deleteRow) => {
    axios.get(`${BASE_END_POINT}reports?page=${page}&limit={20}`,{
       headers: {
         'Content-Type': 'application/json',
         'Authorization': `Bearer ${this.props.currentUser.token}`
       },
     })
    .then(response=>{
      console.log("ALL actions")
      console.log(response.data.data)
      if(deleteRow){
        this.setState({tablePage:0})
       }
      this.setState({actions:deleteRow?response.data.data:[...this.state.actions,...response.data.data],loading:false})
      //console.log(this.state.actions)
      this.page +=1
    })
    .catch(error=>{
      this.setState({loading:false})
      console.log("ALL actions ERROR")
      console.log(error.response)
    })
  }

  
   OKBUTTON = (e) => {
    this.deleteActions()
   }

   deleteActions = () => {
     let l = message.loading(allStrings.wait, 2.5)
     axios.delete(`${BASE_END_POINT}reports/${this.state.selectedAction}`,{
       headers: {
         'Content-Type': 'application/x-www-form-urlencoded',
         'Authorization': `Bearer ${this.props.currentUser.token}`
       },
     })
     .then(response=>{
         l.then(() => message.success(allStrings.deleteDone, 2.5));
         this.getAction(1,true)
         this.flag = -1
     })
     .catch(error=>{
        // console.log(error.response)
         l.then(() => message.error('Error', 2.5))
     })
  }
  
    render() {
      const{select} = this.props
      let controls = (
        <Popconfirm
        title={allStrings.areYouSure}
        onConfirm={this.OKBUTTON}
        onCancel={this.fCANCELBUTTON}
        okText={allStrings.ok}
        cancelText={allStrings.cancel}
      >
         <Icon className='controller-icon' type="delete" />
      </Popconfirm>
     )

     let list = this.state.actions.map(val=>
      [val.id,val.action,val.user?val.user.firstname +' '+ val.user.lastname:'',val.createdAt.substring(0,10)]
      );      
     
     list.forEach(function(row) {
          row.push(controls)
        });
        const loadingView = [
          [<Skeleton  active/> ],
          [<Skeleton active/> ],
          [<Skeleton  active/> ],
          [<Skeleton active/> ],
          
         ]


      return (
          <div>
               <AppMenu  height={'115%'} goTo={this.props.history}></AppMenu>
              <Nav></Nav>
              <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}} className='menu'>
                
              <Tables
               columns={this.state.loading?['Loading...']:[allStrings.id,allStrings.action, allStrings.user,allStrings.date,allStrings.remove]} 
               title={allStrings.reportsTable}
               page={this.state.tablePage}
               onCellClick={(colData,cellMeta,)=>{
                //console.log('col index  '+cellMeta.colIndex)
                //console.log('row index   '+colData.rowIndex)
                if(cellMeta.colIndex===4){
                    const id = list[ this.pagentationPage+cellMeta.rowIndex][0];
                    this.setState({selectedAction:id})
                    
                  }
              }}
              onChangePage={(currentPage)=>{
                this.setState({tablePage:currentPage})
                if(currentPage>this.counter){
                  this.counter=currentPage;
                  this.pagentationPage=this.pagentationPage+10
                }else{
                 this.counter=currentPage;
                 this.pagentationPage=this.pagentationPage-10
                }
                //console.log(currentPage)
                if(currentPage%2!==0  && currentPage > this.flag){
                  this.getAction(currentPage+1)
                  this.flag  = currentPage;
                 
                }
                  
              }}
              arr={this.state.loading?loadingView:list}
              ></Tables>
              </div>
          </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,


  })
  
  const mapDispatchToProps = {
  }

export default connect(mapToStateProps,mapDispatchToProps) (Report);

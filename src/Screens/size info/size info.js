import React from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import './size info.css';
import {Carousel} from 'react-materialize';
import "antd/dist/antd.css";
 import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import {  message,Modal, Form, Input,Icon} from 'antd';
import {Table} from 'react-materialize'

class SizeInfo extends React.Component {
    state = {
        modal1Visible: false,
         Size:this.props.location.state.data,
         flag:this.props.location.state.flag,
         file: this.props.location.state.data.img,
         }

         constructor(props){
          super(props)
          if(this.props.isRTL){
            allStrings.setLanguage('ar')
          }else{
            allStrings.setLanguage('en')
          }
        }
        
         onChange = (e) => {
            this.setState({file:e.target.files[0]});
        }
        
         componentDidMount()
         {
             //console.log("ANWEr")
             //console.log(this.props.currentUser)
            
             //this.props.location.state.data
             //console.log(this.props.location.state.data)
         }
   
    
    deleteSize = () => {
        let l = message.loading(allStrings.wait, 2.5)
        axios.delete(`${BASE_END_POINT}size/${this.state.Size.id}`,{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
        .then(response=>{
            l.then(() => message.success(allStrings.deleteDone, 2.5))
            this.props.history.goBack()

        })
        .catch(error=>{
            console.log(error.response)
            l.then(() => message.error('Error', 2.5))
        })
     }

     handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          //console.log('Received values of form: ', values);
          var form = new FormData();
        
          form.append('size', values.size);
          form.append('arabicSize', values.arabicSize);
          let data ={
            size:values.size,
            arabicSize:values.arabicSize,
            sizeFrom:values.sizeFrom,
            sizeTo:values.sizeTo,
            price:values.price
          }
          console.log(data)
    
          let l = message.loading(allStrings.wait, 2.5)
          axios.put(`${BASE_END_POINT}size/${this.state.Size.id}`,JSON.stringify(data),{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
          .then(response=>{
              l.then(() => message.success(allStrings.updatedDone, 2.5))
              this.props.history.goBack()
          })
          .catch(error=>{
           //   console.log(error.response)
              l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
          })
        }
      });
      
    }

      
      //modal
     
      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }
    render() {
        const { getFieldDecorator } = this.props.form;
         //select
         const {Size} = this.state

        const {select} = this.props;
      return (
          
        <div>
         <AppMenu height={'150%'} goTo={this.props.history}></AppMenu>
          <Nav></Nav>
          <div style={{marginRight:!select?'20.2%':'5.5%'}} className='menu'>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#001529'}} >
                    <h2 class="center-align" style={{color:'#fff',marginTop:"10px"}}>{allStrings.SizeInfo}</h2>
                </div>
                
                <div class="row" style={{marginTop:"0px"}}>
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={Size.id}>
                            </input>
                            <label for="name" class="active">{allStrings.id}</label>
                            </div>

                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={Size.size}>
                            </input>
                            <label for="name" class="active">{allStrings.EnglishSizeName}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={Size.arabicSize}>
                            </input>
                            <label for="name" class="active">{allStrings.arabicSizeName}</label>
                            </div>

                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={Size.sizeFrom}>
                            </input>
                            <label for="name" class="active">{allStrings.from}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={Size.sizeTo}>
                            </input>
                            <label for="name" class="active">{allStrings.to}</label>
                            </div>

                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={Size.price}>
                            </input>
                            <label for="name" class="active">{allStrings.delivaryCost}</label>
                            </div>


                        </div>


               
                       
                            <a class="waves-effect waves-light btn btn-large delete " onClick={this.deleteSize}><i class="material-icons left spcial">delete</i>{allStrings.remove}</a>
                            <a class="waves-effect waves-light btn btn-large edit" onClick={() => this.setModal1Visible(true)}><i class="material-icons left spcial">edit</i>{allStrings.edit}</a>



                            <div>
                            <Modal
                                title={allStrings.edit}
                                visible={this.state.modal1Visible}
                                onOk={this.handleSubmit}
                                okText={allStrings.ok}
                                cancelText={allStrings.cancel}
                                onCancel={() => this.setModal1Visible(false)}
                              >
                              
                               <Form onSubmit={this.handleSubmit} className="login-form">


                                  <label for="name" class="lab">{allStrings.size}</label>
                                  <Form.Item>
                                  {getFieldDecorator('size', {
                                      rules: [{ required: true, message: 'Please enter size' }],
                                      initialValue: Size.size
                                  })(
                                      <Input  />
                                  )}
                                  </Form.Item>
                                  <Form.Item>
                                  {getFieldDecorator('arabicSize', {
                                      rules: [{ required: true, message: 'Please enter arabic size' }],
                                      initialValue: Size.arabicSize
                                  })(
                                      <Input  />
                                  )}
                                  </Form.Item>
                                  <Form.Item>
                                  {getFieldDecorator('sizeFrom', {
                                      rules: [{ required: true, message: 'Please enter low size' }],
                                      initialValue: Size.sizeFrom
                                  })(
                                      <Input  />
                                  )}
                                  </Form.Item>
                                  <Form.Item>
                                  {getFieldDecorator('sizeTo', {
                                      rules: [{ required: true, message: 'Please enter higher size' }],
                                      initialValue: Size.sizeTo
                                  })(
                                      <Input  />
                                  )}
                                  </Form.Item>
                                  <Form.Item>
                                  {getFieldDecorator('price', {
                                      rules: [{ required: true, message: 'Please enter dilivery cost' }],
                                      initialValue: Size.price
                                  })(
                                      <Input  />
                                  )}
                                  </Form.Item>
                           
                                
                                 
                              </Form>
                            </Modal>

                                
                        
                            </div>
                        </form>
                        
                    </div>
            </div>
        </div>
        </div>
        <Footer></Footer>
        </div>
    </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,
  })
  
  const mapDispatchToProps = {
  }

export default connect(mapToStateProps,mapDispatchToProps)(SizeInfo = Form.create({ name: 'normal_login', })(SizeInfo)) ;

import React from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import Tables from '../../components/table/table';
import './product.css';
import { Tag, Input,Skeleton,Modal, Form, Icon,Button, Popconfirm, message,Select} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import {withRouter} from 'react-router-dom'
import  {allStrings} from '../../assets/strings'
class Product extends React.Component {
    //submit form
    pagentationPage=0;
    counter=0;
    state = {
      selectedSizes:'',
      modal1Visible: false,
      confirmDelete: false,
        selectedproducts:null,
      products:[],
      sizes:[],
      loading:true,
      file: null,
      deletedRow:null,
      tablePage:0,
      categories:[],
      companyName:'',
      inputValue:'',
      Colors: [],
      colorInput: false,
      colorValue: '',
      sizesCustom: [],
      sizesInput: false,
      sizesValue: '',
      }
      
  handelColorClose = removedTag => {
    const Colors = this.state.Colors.filter(tag => tag !== removedTag);
    console.log(Colors);
    this.setState({ Colors });
  };

  showColorInput = () => {
    this.setState({ colorInput: true }, () => this.input.focus());
  };

  handelInputColorChange = e => {
    this.setState({ colorValue: e.target.value });
  };

  handelInputColorConfirm = () => {
    const { colorValue } = this.state;
    let { Colors } = this.state;
    if (colorValue && Colors.indexOf(colorValue) === -1) {
      Colors = [...Colors, colorValue];
    }
    console.log(Colors);
    this.setState({
      Colors,
      colorInput: false,
      colorValue: '',
    });
  };

  saveInputColorRef = input => (this.input = input);

  forColorMap = tag => {
    const tagElem = (
      <Tag
        closable
        onClose={e => {
          e.preventDefault();
          this.handelColorClose(tag);
        }}
      >
        {tag}
      </Tag>
    );
    return (
      <span key={tag} style={{ display: 'inline-block' }}>
        {tagElem}
      </span>
    );
  };
//
handelsizesClose = removedTag => {
  const sizesCustom = this.state.sizesCustom.filter(tag => tag !== removedTag);
  console.log(sizesCustom);
  this.setState({ sizesCustom });
};

showsizesInput = () => {
  this.setState({ sizesInput: true }, () => this.input.focus());
};

handelInputsizesChange = e => {
  this.setState({ sizesValue: e.target.value });
};

handelInputsizesConfirm = () => {
  const { sizesValue } = this.state;
  let { sizesCustom } = this.state;
  if (sizesValue && sizesCustom.indexOf(sizesValue) === -1) {
    sizesCustom = [...sizesCustom, sizesValue];
  }
  console.log(sizesCustom);
  this.setState({
    sizesCustom,
    sizesInput: false,
    sizesValue: '',
  });
};

saveInputsizesRef = input => (this.input = input);

forsizesMap = tag1 => {
  const tagElem1 = (
    <Tag
      closable
      onClose={e => {
        e.preventDefault();
        this.handelsizesClose(tag1);
      }}
    >
      {tag1}
    </Tag>
  );
  return (
    <span key={tag1} style={{ display: 'inline-block' }}>
      {tagElem1}
    </span>
  );
};
getCategories = (page) => {
  axios.get(`${BASE_END_POINT}categories?page=${page}&limit={20}`)
  .then(response=>{
    //console.log("ALL Categories")
    //console.log(response.data)
    this.setState({categories:response.data.data})
  })
  .catch(error=>{
    console.log("ALL Categories ERROR")
    console.log(error.response)
  })
}
        getSize = (page) => {
          axios.get(`${BASE_END_POINT}size?page=${page}&limit={20}`)
          .then(response=>{
          //  console.log("ALL Categories")
         // console.log(response.data)
       
            this.setState({sizes:response.data.data})
          })
          .catch(error=>{
          console.log("ALL Categories ERROR")
            console.log(error.response)
          })
        }

      constructor(props){
        super(props)
        this.getproducts(1,true)
        if(this.props.isRTL){
          allStrings.setLanguage('ar')
        }else{
          allStrings.setLanguage('en')
        }
        this.updateInput = this.updateInput.bind(this);

      }
      onChangeImg = (e) => {
        //console.log(Array.from(e.target.files))
        this.setState({file:e.target.files[0]});
       // console.log(e.target.files[0])
    }
     //submit form
     flag = 0;

     getproducts = (page,deleteRow) => {
      
       axios.get(`${BASE_END_POINT}products?page=${page}&limit={20}`)
       .then(response=>{
         //console.log("ALL products")
         console.log(response.data)
         if(deleteRow){
          this.setState({tablePage:0})
         }
         this.setState({products:deleteRow?response.data.data:[...this.state.products,...response.data.data],loading:false})
         this.page +=1
        })
       .catch(error=>{
        this.setState({loading:false})
         console.log("ALL products ERROR")
         console.log(error.response)
       })
     }
     getproductsfilter = (page,deleteRow,companyName) => {
      console.log(this.state.inputValue)
      axios.get(`${BASE_END_POINT}products/?company=${this.state.inputValue}&page=${page}&limit={20}`)
      .then(response=>{
        //console.log("ALL products")
        //console.log(response.data)
        if(deleteRow){
         this.setState({tablePage:0})
        }
        this.setState({products:deleteRow?response.data.data:[...this.state.products,...response.data.data],loading:false})
        document.getElementById('searchCompany').value = ''
      })
      .catch(error=>{
       this.setState({loading:false})
        console.log("ALL products ERROR")
        console.log(error.response)
      })
    }

 
     componentDidMount(){
       
      //console.log('this.props.currentUser')
      // console.log(this.props.currentUser)
      
       this.getCategories()
       this.getSize()
       var form = new FormData();
      form.append('my_field', 'my value');

      // console.log('data')
      // console.log(form)
     
     }
     OKBUTTON = (e) => {
      this.deleteproducts()
     }

     deleteproducts = () => {
       let l = message.loading('Wait..', 2.5)
       //console.log(this.state.selectedproducts);
       axios.delete(`${BASE_END_POINT}products/${this.state.selectedproducts}`,{
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${this.props.currentUser.token}`
        },
       })
       .then(response=>{
           l.then(() => message.success('products Deleted', 2.5));
           this.getproducts(1,true)
             this.flag = -1
           
       })
       .catch(error=>{
           //console.log(error.response)
           l.then(() => message.error('Error', 2.5))
       })
    }
    
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
         
            var form = new FormData();
            form.append('img',this.state.file);
            form.append('name', values.name);
            form.append('price', values.price);
            form.append('description', values.description);
            form.append('quantity', values.quantity);
            form.append('company', values.company);
            form.append('category', values.category.key);
            form.append('size', values.size.key);
            if(this.state.Colors.length >0){
              form.append('color', JSON.stringify(this.state.Colors));
            }
            if(this.state.sizesCustom.length >0){
              form.append('sizes', JSON.stringify(this.state.sizesCustom));
            }
            
            
           
            /*
            for(var i=0;i<values.size.length;i=i+1){
              form.append('size', values.size[i]);

            }
            
*/
            

            let l = message.loading('Wait..', 2.5)
            axios.post(`${BASE_END_POINT}products`,form,{
              headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${this.props.currentUser.token}`
              },
            })
            .then(response=>{
                l.then(() => message.success('Add Product', 2.5));
                this.setState({ modal1Visible:false });
                this.getproducts(1,true)
                this.flag = -1
                this.props.form.resetFields()

            })
            .catch(error=>{
                //console.log(error.response)
                l.then(() => message.error('Error', 2.5))
            })
  
       /* if(values.img.fileList.length>0){
          values.img.fileList.filter(val=>{
            data.append('img',{
              uri: val.response.url,
              type: 'multipart/form-data',
              name: 'productImages'
          }) 
          })
           
        }*/ 
        
          }
        });
        
      }
  
        
        //modal
       
        setModal1Visible(modal1Visible) {
          this.setState({ modal1Visible });
        } 
        updateInput(e){
          this.setState({inputValue : e.target.value})
          }
     
    render() {
      function handleChange(value) {
        console.log(` ${value}`);
      }
        //form
         const { getFieldDecorator } = this.props.form; 
          //end upload props
          const Option = Select.Option;
          let controls = (
            <Popconfirm
            title={allStrings.areusure}
            onConfirm={this.OKBUTTON}
            onCancel={this.fCANCELBUTTON}
            okText={allStrings.yes}
            cancelText={allStrings.cancel}
          >
             <Icon className='controller-icon' type="delete" />
          </Popconfirm>
         )

         let list =this.state.products.map((val,index)=>[
          val.id,""+val.name,""+val.price,
          ""+val.quantity,""+val.category.arabicname,""+val.company,
          ""+val.available,controls
        ])

        const loadingView = [
          [<Skeleton  active/> ],
          [<Skeleton active/> ],
          [<Skeleton  active/> ],
          [<Skeleton active/> ],
          
         ]
const {select} = this.props;
const { Colors, colorInput, colorValue,sizesCustom, sizesInput, sizesValue } = this.state;
    const colorsChild = Colors.map(this.forColorMap);
    const sizesChild = sizesCustom.map(this.forsizesMap);
      return (
          <div>
              <AppMenu  height={'140%'} goTo={this.props.history}></AppMenu>
              <Nav></Nav>
              
              <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}} className='menu'>
             <div className='filterCustom'>
              <div class="row">
                
                <div class="input-field col s12" style={{width: '90%', marginRight:'2%'}}>
                  <Input id="searchCompany" style={{width: '90%', marginRight:'2rem',marginLeft:'2rem'}} type='text' name='companyFilter' placeholder={allStrings.searchCompany} onChange={this.updateInput}></Input>
                  <Button style={{color: 'white', backgroundColor:'#3497fd', width: '85%', marginRight:'1rem',marginLeft:'1rem',height:'40px'}} onClick={() => this.getproductsfilter(1,`${this.state.inputValue}`)}>{allStrings.search}</Button>
                  <Icon className='controller-icon' type="undo" onClick={() => this.getproducts(1)} />
                </div>
              </div>
              </div>
              <Tables
               columns={this.state.loading?['loading...'] :[allStrings.id,allStrings.name,allStrings.price,allStrings.count,allStrings.category,allStrings.company,allStrings.active, allStrings.remove]} 
              title={allStrings.productTable}
              page={this.state.tablePage}
              onCellClick={(colData,cellMeta,)=>{
                //console.log('col index  '+cellMeta.colIndex)
                //console.log('row index   '+colData)
                if(cellMeta.colIndex!==7){
                  
                  //console.log(this.state.products[cellMeta.rowIndex])
                  this.props.history.push('/ProductInfo',{data:this.state.products[this.pagentationPage+cellMeta.rowIndex]})
                }else if(cellMeta.colIndex===7){
                  const id = list[this.pagentationPage +cellMeta.rowIndex][0];
                  this.setState({selectedproducts:id,deletedRow:cellMeta.rowIndex})
                    console.log(id)
                  }
              }}
               onChangePage={(currentPage)=>{
                this.setState({tablePage:currentPage})
                if(currentPage>this.counter){
                  this.counter=currentPage;
                  this.pagentationPage=this.pagentationPage+10
                }else{ 
                 this.counter=currentPage;
                 this.pagentationPage=this.pagentationPage-10
                }
                console.log('current',currentPage)
              
                if(currentPage%2!=0  && currentPage > this.flag){
                  let rem = currentPage %2;
                  let x = currentPage - rem;
                  console.log(rem)
                  console.log(x)
                  this.flag  = this.flag +1;
                  console.log("flage",this.flag)
                    let page = rem + this.flag;
                    console.log("page",page)
                    this.getproducts(page)
                    console.log("request",page)
                    console.log("flag",this.flag) 

                } 
               

                  
              }}
              arr={this.state.loading? loadingView:list}></Tables>
              <div>
              <Button style={{color: 'white', backgroundColor:'#3497fd', marginLeft:60}} onClick={() => this.setModal1Visible(true)}>{allStrings.addproduct}</Button>
              <Modal
                    title={allStrings.addproduct}
                    visible={this.state.modal1Visible}
                    onOk={this.handleSubmit}
                    onCancel={() => this.setModal1Visible(false)}
                    okText={allStrings.ok}
                    cancelText={allStrings.cancel}
                  >
                    <Form onSubmit={this.handleSubmit} className="login-form">
                    <Form.Item>
                      {getFieldDecorator('name', {
                          rules: [{ required: true, message: 'Please enter product anme' }],
                      })(
                          <Input placeholder={allStrings.name}/>
                      )}
                      </Form.Item>
                      <Form.Item>
                      {getFieldDecorator('price', {
                          rules: [{ required: true, message: 'Please enter product price' }],
                      })(
                          <Input placeholder={allStrings.price}/>
                      )}
                      </Form.Item>
                      <Form.Item>
                      {getFieldDecorator('description', {
                          rules: [{ required: true, message: 'Please enter description' }],
                      })(
                          <Input placeholder={allStrings.description}/>
                      )}
                      </Form.Item>
                      <Form.Item>
                      {getFieldDecorator('company', {
                          rules: [{ required: true, message: 'Please enter company' }],
                      })(
                          <Input placeholder={allStrings.company}/>
                      )}
                      </Form.Item>
                      <Form.Item>
                      {getFieldDecorator('quantity', {
                          rules: [{ required: true, message: 'Please enter count' }],
                      })(
                          <Input placeholder={allStrings.count} type='number'/>
                      )}
                      </Form.Item>
                     
                      <Form.Item>
                      {getFieldDecorator('category', {
                          rules: [{ required: true, message: 'Please enter category' }],
                      })(
                          <Select labelInValue  
                          placeholder={allStrings.category}
                          style={{ width: '100%'}} >
                          {this.state.categories.map(
                              val=><Option value={val.id}>{val.categoryname}</Option>
                          )}
                          </Select>
                      )}
                      </Form.Item>
                      <Form.Item>
                      {getFieldDecorator('size', {
                          rules: [{ required: true, message: 'Please enter size' }],
                      })(
                          <Select labelInValue  
                          placeholder={allStrings.size}
                          style={{ width: '100%'}} >
                          {this.state.sizes.map(
                              val=><Option value={val.id}>{val.size}</Option>
                          )}
                          </Select>
                      )}
                      </Form.Item>
                      <div>
                        <div style={{ marginBottom: 16 }}>
                            {colorsChild}
                        </div>
                        {colorInput && (
                          <Input
                            ref={this.saveInputColorRef}
                            type="text"
                            style={{ width: '100%' }}
                            value={colorValue}
                            onChange={this.handelInputColorChange}
                            onBlur={this.handelInputColorConfirm}
                            onPressEnter={this.handelInputColorConfirm}
                          />
                        )}
                        {!colorInput && (
                          <Tag onClick={this.showColorInput} className="site-tag-plus">
                            + {allStrings.addColor}
                          </Tag>
                        )}
                      </div>
                      <div>
                        <div style={{ marginBottom: 16 }}>
                            {sizesChild}
                        </div>
                        {sizesInput && (
                          <Input
                            ref={this.saveInputsizesRef}
                            type="text"
                            style={{ width: '100%' }}
                            value={sizesValue}
                            onChange={this.handelInputsizesChange}
                            onBlur={this.handelInputsizesConfirm}
                            onPressEnter={this.handelInputsizesConfirm}
                          />
                        )}
                        {!sizesInput && (
                          <Tag onClick={this.showsizesInput} className="site-tag-plus">
                            + {allStrings.addSize}
                          </Tag>
                        )}
                      </div>
                     
                      {/*}
                      <Form.Item>
                      {getFieldDecorator('size', {
                          rules: [{ required: true, message: 'Please enter size' }],
                      })(
                        <Select
                        mode="multiple"
                        style={{ width: '100%' }}
                        placeholder="select size"
                        onChange={handleChange}
                        optionLabelProp="label" 
                      >
                         {this.state.sizes.map(
                              val=><Option value={val.id} label={val.size}>{val.size} </Option>
                          )}
                        
                      </Select>
                      )}
                      </Form.Item>
                         */}
                      
                      <br></br>
                      <Form.Item>
                      {getFieldDecorator('img', {
                          rules: [{ required: true, message: 'Please upload img' }],
                      })(
                          <input type="file" onChange= {this.onChangeImg} multiple></input>
                      )}
                          
                      </Form.Item>
                    </Form>
                </Modal>
                
            </div>
            <Footer></Footer>
             </div>
          </div>
      );
    }
  } 

  
  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,
  })
  
  const mapDispatchToProps = {

  }

  export default  withRouter(connect(mapToStateProps,mapDispatchToProps)(Product = Form.create({ name: 'normal_login' })(Product)));

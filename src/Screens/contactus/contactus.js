import React from 'react';
import AppMenu from '../../components/menu/menu';

import Nav from '../../components/navbar/navbar';

import './contactus.css';
import { message,Modal, Form, Icon, Input,Avatar} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'


class Contactus extends React.Component {
  page = 1;
  pagentationPage=0;
  counter=0;
  state = {
    modal1Visible: false,
    confirmDelete: false,
    selectedCategory:null,
    selectedMessage:null,
     messages:[],
     file:null,
     loading:true,
     }

     constructor(props){
      super(props)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }

     onChange = (e) => {
      this.setState({file:e.target.files[0]});
     
  }
    //submit form
    flag = -1;
    getMessages= (page,deleteRow) => {
      axios.get(`${BASE_END_POINT}contact-us?page=${page}&limit=20`,{
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${this.props.currentUser.token}`
        },
      })
      .then(response=>{
        console.log("ALL messages     ",response.data)
        //console.log(response.data)
        
        this.setState({messages:deleteRow?response.data.data:[...this.state.messages,...response.data.data],loading:false})
      })
      .catch(error=>{
        console.log("ALL messages ERROR")
        console.log(error.response)
        this.setState({loading:false})
      })
    }
    componentDidMount(){
      this.getMessages(1)
      setTimeout(()=>{
        this.getMessages(1,true)
      },1000*60*2)
    }
    

     deleteMessages = (id) => {
       let l = message.loading(allStrings.wait, 2.5)
       axios.delete(`${BASE_END_POINT}/contact-us/${id}`,{
         headers: {
           'Content-Type': 'application/json',
           'Authorization': `Bearer ${this.props.currentUser.token}`
         },
       })
       .then(response=>{
           l.then(() => message.success(allStrings.deleteDone, 2.5))
           this.getMessages(1,true)
           this.flag = -1
       })
       .catch(error=>{
          // console.log(error.response)
           l.then(() => message.error('Error', 2.5))
       })
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
            const data = {
              reply:values.replay
            }
            let l = message.loading(allStrings.wait, 2.5)
            console.log("id   ",this.state.selectedMessage)
            axios.post(`${BASE_END_POINT}contact-us/${this.state.selectedMessage.id}/reply`,JSON.stringify(data),{
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.props.currentUser.token}`
              },
            })
            .then(response=>{
                l.then(() => message.success(allStrings.replyDone, 2.5));
                this.setState({ modal1Visible:false });
                
            })
            .catch(error=>{
               console.log("message error    ",error.response)
                l.then(() => message.error('Error', 2.5))
            })
          }
        }); 
        
      }

      componentDidUpdate(){
        console.log("data   ",this.state.selectedMessage)
      }
     
    //end submit form

      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }

//end modal
    render() {
        //form
         const { getFieldDecorator } = this.props.form;
        
          
        
           const {select} = this.props;
      return (
          <div>
              <AppMenu height={'140%'} goTo={this.props.history}></AppMenu>
              <Nav></Nav>
              <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}} className='menu'>
              
              <div onClick={()=>{              
                this.getMessages(this.page+1)
                this.page=this.page+1
                alert(this.page)
              }} style={{cursor:'pointer', marginTop:30, display:'flex', flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
                    <span style={{color:'green'}}>Load More   </span>
                    <Icon  className='controller-icon' type="plus" />

              </div>

                {this.state.messages.map(val=>
                
                <div style={{marginTop:10,marginLeft:' 6%'}}>
                  
                <div style={{display:'flex', flexDirection:'row',alignItems:'center'}}>
                    <div style={{marginLeft:10}} >
                    <Avatar size={70} src='https://img.icons8.com/bubbles/2x/user.png' />
                    </div>
                    <div style={{marginLeft:10}}>
                        <span style={{fontSize:15}}>{val.name}</span>
                        <br/>
                        <span style={{fontSize:12}}>{val.number}</span>
                    </div>
                </div>
                <div style={{marginLeft:'14%',marginRight:'14%'}}>
                <span style={{color:'red'}} >{val.message}</span>
                </div>

                <div style={{marginRight:'13%',display:'flex', flexDirection:'row',justifyContent:'flex-end'}}>
                <Icon onClick={()=>{this.deleteMessages(val.id)}} className='controller-icon' type="delete" />
                <Icon onClick={()=>{this.setState({modal1Visible:true,selectedMessage:val})}} className='controller-icon' type="message" />
                </div>
                  <hr></hr>
                </div>
                )}
                
              
              <Modal
                    title={allStrings.replay}
                    visible={this.state.modal1Visible}
                    onOk={this.handleSubmit}
                    onCancel={() => this.setModal1Visible(false)}
                    okText={allStrings.ok}
                    cancelText={allStrings.cancel}
                  >
                    <Form onSubmit={this.handleSubmit} className="login-form">
                       <div style={{marginBottom:40, textAlign:'center'}}>
                         {this.state.selectedMessage&&this.state.selectedMessage.message}
                       </div>
                        <Form.Item>
                        {getFieldDecorator('replay', {
                            rules: [{ required: true, message: 'Please enter write message' }],
                        })(
                            <Input placeholder={allStrings.replay}/>
                        )}
                        </Form.Item>
                    </Form>
                </Modal>

              </div>
             
          </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(Contactus = Form.create({ name: 'normal_login' })(Contactus));

import React from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import './city info.css';
import "antd/dist/antd.css";
 import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
import {  message,Modal, Form, Input,Icon} from 'antd';
import {Table} from 'react-materialize'

class CityInfo extends React.Component {
    state = {
        modal1Visible: false, 
        modal2Visible: false,
         areas:[],
         
         City:this.props.location.state.data,
         flag:this.props.location.state.flag,
         file: null,
         }

         constructor(props){
          super(props)

          if(this.props.isRTL){
            allStrings.setLanguage('ar')
          }else{
            allStrings.setLanguage('en')
          }
          this.getareas()
        }
        
         onChange = (e) => {
            this.setState({file:e.target.files[0]});
        }
        getareas = () => {
          axios.get(`${BASE_END_POINT}cities/${this.state.City.id}/areas`)
          .then(response=>{
            console.log(response.data)
            this.setState({areas:response.data})
          })
          .catch(error=>{
            //console.log("ALL Categories ERROR")
            //console.log(error.response)
          })
        }
      
        
         
    
    deleteCity = () => {
        let l = message.loading(allStrings.wait, 2.5)
        axios.delete(`${BASE_END_POINT}cities/${this.state.City.id}`,{
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
        .then(response=>{
            l.then(() => message.success(allStrings.deleteDone, 2.5))
            this.props.history.goBack()

        })
        .catch(error=>{
            console.log(error.response)
            l.then(() => message.error('Error', 2.5))
        })
     }

     deleteArea = (id) => {
      let l = message.loading(allStrings.wait, 2.5)
      axios.delete(`${BASE_END_POINT}cities/${this.state.City.id}/areas/${id}`,{
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${this.props.currentUser.token}`
        },
      })
      .then(response=>{
          l.then(() => message.success(allStrings.deleteDone, 2.5))
          this.props.history.goBack()

      })
      .catch(error=>{
          console.log(error.response)
          l.then(() => message.error('Error', 2.5))
      })
   }

     handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFieldsAndScroll(['cityName','arabicCityName','delivaryCost'],(err, values)  => {
        if (!err) {
          //console.log('Received values of form: ', values);
         
            var data ={
            "cityName": values.cityName,
            "arabicCityName": values.arabicCityName,
            "delivaryCost":values.delivaryCost
           }
           
           var form = new FormData();
      /*
          form.append('CityName', values.cityName);
          form.append('arabicCityName', values.arabicName);
          form.append('delivaryCost', values.delivaryCost);
    */
          let l = message.loading(allStrings.wait, 2.5)
          axios.put(`${BASE_END_POINT}cities/${this.state.City.id}`,JSON.stringify(data),{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
          .then(response=>{
              l.then(() => message.success(allStrings.updatedDone, 2.5))
              this.props.history.goBack()
          })
          .catch(error=>{
           //   console.log(error.response)
              l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
          })
        }
      });
      
    }
    
    handleSubmitAdd = (e) => {
      e.preventDefault();
      this.props.form.validateFieldsAndScroll(['areaName','arabicAreaName','delivaryCost'],(err, values) => {
        if (!err) {
          //console.log('Received values of form: ', values);
         var data ={
          "areaName": values.areaName,
          "arabicAreaName": values.arabicAreaName,
          "delivaryCost":values.delivaryCost
         }
          
          let l = message.loading(allStrings.wait, 2.5)
          axios.post(`${BASE_END_POINT}cities/${this.state.City.id}/areas`,JSON.stringify(data),{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
          .then(response=>{
              l.then(() => message.success(allStrings.updatedDone, 2.5))
              this.props.history.goBack()
          })
          .catch(error=>{
           //   console.log(error.response)
              l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
          })
        }
      });
      
    }


      
      //modal
     
      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }
      setModal2Visible(modal2Visible) {
        this.setState({ modal2Visible });
      }
    render() {
        const { getFieldDecorator } = this.props.form;
         //select
         const {City} = this.state
         const {areas} = this.state.areas
         console.log(this.state.areas)
  
        const {select} = this.props;
      return (
          
        <div>
         <AppMenu height={'150%'} goTo={this.props.history}></AppMenu>
          <Nav></Nav>
          <div style={{marginRight:!select?'20.2%':'5.5%'}} className='menu'>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#001529'}} >
                    <h2 class="center-align" style={{color:'#fff',marginTop:"10px"}}>{allStrings.CityInfo}</h2>
                </div>
               
                <div class="row" style={{marginTop:"0px"}}>
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={City.cityName}>
                            </input>
                            <label for="name" class="active">{allStrings.EnglishCityName}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={City.arabicCityName}>
                            </input>
                            <label for="name" class="active">{allStrings.ArabicCityName}</label>
                            </div>

                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="name" type="text" class="validate" disabled value={City.delivaryCost}>
                            </input>
                            <label for="name" class="active">{allStrings.delivaryCost}</label>
                            </div>

                           

                        </div>
                        <div className='dash-table'>
                            <h5>{allStrings.areas} : </h5>
                            <div className='row'>
                                <div className="col s6 m6 l6 dashboard-table">
                                    <Table>
                                        <thead>
                                            <tr>
                                            <th data-field="id">{allStrings.id}</th>
                                            <th data-field="name">{allStrings.areaName}</th>
                                            <th data-field="arabic">{allStrings.ArabicAreaName}</th>
                                            <th>{allStrings.remove}</th>
                                            </tr>
                                        </thead> 

                                        <tbody>
                                        {this.state.areas.map(val=>(
                                            <tr>
                                                <td  onClick={()=>{this.props.history.push('/AreaInfo',{data:val}) }} >{val.id}</td>
                                                <td  onClick={()=>{this.props.history.push('/AreaInfo',{data:val}) }} >{val.areaName}</td>                                 
                                                <td  onClick={()=>{this.props.history.push('/AreaInfo',{data:val}) }} >{val.arabicAreaName}</td>
                                                <td 
                                                onClick={()=>{this.deleteArea(val.id)}}
                                                 ><Icon className='controller-icon' type="delete" style={{ fontSize: "16px"}}/></td>
                                            </tr>
                                        ))}

                                        </tbody>
                                    </Table>
                                </div>
                            </div>
                        </div> 

                        
                       
                            <a className="waves-effect waves-light btn btn-large delete " onClick={this.deleteCity}><i class="material-icons left spcial">delete</i>{allStrings.remove}</a>
                            <a className="waves-effect waves-light btn btn-large edit" onClick={() => this.setModal1Visible(true)}><i class="material-icons left spcial">edit</i>{allStrings.edit}</a>
                            <a style={{background:"#2bbbad"}} className="waves-effect waves-light btn btn-large " onClick={() => this.setModal2Visible(true)}><i class="material-icons left spcial">add</i>{allStrings.addArea}</a>



                            <div>
                            <Modal
                                title={allStrings.edit}
                                visible={this.state.modal1Visible}
                                onOk={this.handleSubmit}
                                okText={allStrings.ok}
                                cancelText={allStrings.cancel}
                                onCancel={() => this.setModal1Visible(false)}
                              >
                              
                               <Form onSubmit={this.handleSubmit} className="login-form">

                               <label for="name" className="lab">{allStrings.EnglishCityName}</label>
                                  <Form.Item>
                                  {getFieldDecorator('cityName', {
                                      rules: [{ required: true, message: 'Please enter City name by english' }],
                                      initialValue: City.cityName
                                  })(
                                      <Input type='text' />
                                  )}
                                  </Form.Item>

                                  <label for="name" className="lab">{allStrings.ArabicCityName}</label>
                                  <Form.Item>
                                  {getFieldDecorator('arabicCityName', {
                                      rules: [{ required: true, message: 'Please enter City name by arabic' }],
                                      initialValue: City.arabicCityName
                                  })(
                                      <Input  />
                                  )}
                                  </Form.Item>

                                 

                                  <label for="name" className="lab">{allStrings.delivaryCost}</label>
                                  <Form.Item>
                                  {getFieldDecorator('delivaryCost', {
                                      rules: [{ required: true, message: 'Please enter delivaryCost' }],
                                      initialValue: City.delivaryCost
                                  })(
                                      <Input  />
                                  )}
                                  </Form.Item>

                                 
                           
                                 
                                 
                              </Form>
                            </Modal>

                                
                        
                            </div>

                            
                            <div>
                            <Modal
                                title={allStrings.addArea}
                                visible={this.state.modal2Visible}
                                onOk={this.handleSubmitAdd}
                                okText={allStrings.ok}
                                cancelText={allStrings.cancel}
                                onCancel={() => this.setModal2Visible(false)}
                              >
                              
                               <Form onSubmit={this.handleSubmitAdd} className="login-form">

                                  <Form.Item>
                                  {getFieldDecorator('areaName', {
                                      rules: [{ required: true, message: 'Please enter city name by english' }],
                                      initialValue: ''
                                  })(
                                      <Input placeholder={allStrings.EnglishAreaName}   />
                                  )}
                                  </Form.Item>

                                  <Form.Item>
                                  {getFieldDecorator('arabicAreaName', {
                                      rules: [{ required: true, message: 'Please enter city name by arabic' }],
                                      initialValue: ''
                                  })(
                                      <Input placeholder={allStrings.ArabicAreaName}  />
                                  )}
                                  </Form.Item>
                                  <Form.Item>
                                  {getFieldDecorator('delivaryCost', {
                                      rules: [{ required: true, message: 'Please enter arabic delivaryCost' }],
                                      initialValue: ''
                                  })(
                                      <Input placeholder={allStrings.delivaryCost}/>
                                  )}
                                  </Form.Item>
                           
                                
                                 
                              </Form>
                            </Modal>

                                
                        
                            </div>
                        </form>
                        
                    </div>
            </div>
        </div>
        </div>
        <Footer></Footer>
        </div>
    </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,
  })
  
  const mapDispatchToProps = {
  }

export default connect(mapToStateProps,mapDispatchToProps)(CityInfo = Form.create({ name: 'normal_login', })(CityInfo)) ;

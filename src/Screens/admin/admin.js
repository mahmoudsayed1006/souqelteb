
import React from 'react';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Tables from '../../components/table/table';

import './admin.css';
//import {Icon,Button,Modal,Input} from 'react-materialize';
import {Skeleton,Modal, Form,Select, Icon, Input, Button,  Popconfirm, message} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import {withRouter} from 'react-router-dom'
import { connect } from 'react-redux'
import  {allStrings} from '../../assets/strings'
//

class User extends React.Component {
 
    constructor(props){
      super(props)
      this.getUsers(1)
      this.getcities()
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }

    getcities = () => {
      axios.get(`${BASE_END_POINT}cities`)
      .then(response=>{
        console.log(response.data)
        this.setState({cities:response.data})
      })
      .catch(error=>{
        //console.log("ALL Categories ERROR")
        //console.log(error.response)
      })
    }
    getarea = (id) => {
      axios.get(`${BASE_END_POINT}cities/${id}/areas`)
      .then(response=>{
        this.setState({areas:response.data})
      })
      .catch(error=>{
        //console.log("ALL Categories ERROR")
        //console.log(error.response)
      })
    }
   
    pagentationPage=0;
    counter=0;
    state = {
        modal1Visible: false,
        confirmDelete: false,
        selectedUser:null,
        users:[],
        loading:true,
        countries:[],
        tablePage:0,
        cities:[],
        areas:[],
        }

      flag = -1;
       getUsers = (page,deleteRow) => {
         axios.get(`${BASE_END_POINT}/find?type=ADMIN&page=${page}&limit={20}`)
         .then(response=>{
           console.log("ALL users")
           console.log(response.data)
           if(deleteRow){
            this.setState({tablePage:0})
           }
           this.setState({users:deleteRow?response.data.data:[...this.state.users,...response.data.data],loading:false})
         })
         .catch(error=>{
          // console.log("ALL users ERROR")
           //console.log(error.response)
         })
       }
     
       OKBUTTON = (e) => {
        this.deleteUser()
       }
 
       deleteUser = () => {
         let l = message.loading(allStrings.wait, 2.5)
         axios.delete(`${BASE_END_POINT}${this.state.selectedUser}/delete`,{
           headers: {
             'Content-Type': 'application/x-www-form-urlencoded',
             'Authorization': `Bearer ${this.props.currentUser.token}`
           },
         })
         .then(response=>{
             l.then(() => message.success(allStrings.deleteDone, 2.5))
             this.getUsers(1,true)
             this.flag = -1
         })
         .catch(error=>{
             //console.log(error.response)
             l.then(() => message.error('Error', 2.5))
         })
      }

    //submit form
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
            //console.log('date ', date[2]  );
            const data = {
              firstname: values.firstname,
              lastname: values.lastname,
              phone: values.phone,
              address: values.address,
              email: values.email,
              city: values.city.key,
              area: values.area.key,
              password: values.password,
                type: 'ADMIN',
                                                           
            }
            
            let l = message.loading(allStrings.wait, 2.5)
            axios.post(`${BASE_END_POINT}signup`,JSON.stringify(data),{
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.props.currentUser.token}`
              },
            })
            .then(response=>{
              console.log(allStrings.addDone)
                l.then(() => message.success('Add User', 2.5));
                this.setState({ modal1Visible:false });
                this.getUsers(1,true)
                this.flag = -1
                this.props.form.resetFields()
            })
            .catch(error=>{
              console.log("Add USer Error")
                console.log(error.response)
                l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
            })

          }
        });
        
      }
    //end submit form
      


    showModal = () => {
      this.setState({
        visible: true,
      });
      
    }
    

    setModal1Visible(modal1Visible) {
      this.setState({ modal1Visible });
    }


    setModal2Visible(modal2Visible) {
      this.setState({ modal2Visible });
    }

    validatePhone = (rule, value, callback) => {
      //const { form } = this.props;
      if ( isNaN(value) ) {
        callback('Please enter coorect phone');
      }else if ( value.length <11 ) {
        callback('Phone number must be grater than 11 digit');
      } 
      else {
        callback();
      }
    };
  
  
//end modal//

//end modal

    render() {
      const Option = Select.Option;
      const{select} = this.props
        //form
         const { getFieldDecorator } = this.props.form;
        
         //end select
         let controls = (
            <Popconfirm
            title={allStrings.areYouSure}
            onConfirm={this.OKBUTTON}
            onCancel={this.fCANCELBUTTON}
            okText={allStrings.yes}
            cancelText={allStrings.no}
          >
             <Icon className='controller-icon' type="delete" />
          </Popconfirm>
         )
        
        
         let list = this.state.users.map(val=>[""+val.id,""+val.firstname,""+val.lastname,""+val.phone,""+val.email,
         val.city?val.city.cityName:' ',val.area?val.area.areaName:' ',val.block?allStrings.yes:allStrings.no,controls
        ])
         const loadingView = [
          [<Skeleton  active/> ],
          [<Skeleton active/> ],
          [<Skeleton  active/> ],
          [<Skeleton active/> ],
          
         ]
      return (
          <div>
               <AppMenu  height={'140%'} goTo={this.props.history}></AppMenu>
              <Nav></Nav>
              <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}} className='menu'>
              <Tables 
               page={this.state.tablePage}
              columns={this.state.loading?['Loading...']:[allStrings.id,allStrings.firstname,allStrings.lastname, allStrings.phone,allStrings.email,allStrings.city,allStrings.area,allStrings.block,allStrings.remove]} title={allStrings.adminTable}
              arr={this.state.loading?loadingView:list}
              onCellClick={(colData,cellMeta)=>{
                //console.log('col index  '+cellMeta.colIndex)
                //console.log('row index   '+colData)
                if(cellMeta.colIndex!==8){
                  console.log(colData)
                  console.log('row   ',cellMeta.rowIndex+"         "+cellMeta.dataIndex)
                  this.props.history.push('/UserInfo',{data:this.state.users[this.pagentationPage+cellMeta.rowIndex]})
                }else if(cellMeta.colIndex===8){
                    const id = list[ this.pagentationPage+cellMeta.rowIndex][0];
                    this.setState({selectedUser:id})
                    //console.log(id)
                  }
              }}

              
               onChangePage={(currentPage)=>{
                this.setState({tablePage:currentPage})
                 if(currentPage>this.counter){
                   this.counter=currentPage;
                   this.pagentationPage=this.pagentationPage+10
                 }else{
                  this.counter=currentPage;
                  this.pagentationPage=this.pagentationPage-10
                 }
                console.log(currentPage)
                if(currentPage%2!==0  && currentPage > this.flag){
                  this.getUsers(currentPage+1)
                  this.flag  = currentPage;
                 
                }
                  
              }}
              ></Tables>
              <div>
              <Button style={{color: 'white', backgroundColor:'#25272e', marginLeft:60}}  onClick={() => this.setModal1Visible(true)}>{allStrings.addAdmin}</Button>
              <Modal
                    title={allStrings.addAdmin}
                    visible={this.state.modal1Visible}
                    onOk={this.handleSubmit}
                    okText={allStrings.ok}
                    cancelText={allStrings.cancel}
                    onCancel={() => this.setModal1Visible(false)}
                  >
                    <Form onSubmit={this.handleSubmit} className="login-form">
                    <Form.Item>
                    {getFieldDecorator('firstname', {
                        rules: [{ required: true, message: 'Please enter first Name' }],
                    })(
                        <Input placeholder={allStrings.firstname} />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('lastname', {
                        rules: [{ required: true, message: 'Please enter  last Name' }],
                    })(
                        <Input placeholder={allStrings.lastname} />
                    )}
                    </Form.Item>
                                                        
                    <Form.Item>
                    {getFieldDecorator('email', {
                        rules: [
                          { required: true, message: 'Please enter email' },
                          {type: 'email',  message: 'Please enter correct email' }
                        ],
                    })(
                        <Input placeholder={allStrings.email} />
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('password', {
                        rules: [{ required: true, message: 'Please enter  Password' }],
                    })(
                        <Input placeholder={allStrings.password} />
                    )}
                    </Form.Item>

                    <Form.Item hasFeedback >
                    {getFieldDecorator('phone', {
                        rules: [
                          {required: true, message: 'Please enter phone' },
                          {
                            validator: this.validatePhone,
                          },
                        ],
                        
                    })(
                        <Input placeholder={allStrings.phone} />
                    )}
                    </Form.Item>
                  

                    <Form.Item>
                    {getFieldDecorator('city', {
                        rules: [{ required: true, message: 'Please enter city' }],
                    })(
                      <Select labelInValue  
                      placeholder={allStrings.city}
                      style={{ width: '100%'}} >
                           {this.state.cities.map(val=>
                           <Option  onClick={()=>{this.getarea(val.id)}} value={val.id}>{this.props.isRTL?val.arabicCityName:val.cityName}</Option>
                           )}                 
                      </Select>
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('area', {
                        rules: [{ required: true, message: 'Please enter area' }],
                    })(
                      <Select labelInValue  
                      placeholder={allStrings.area}
                      style={{ width: '100%'}} >
                           {this.state.areas.map(val=>
                           <Option value={val.id}>{this.props.isRTL?val.arabicAreaName:val.areaName}</Option>
                           )}                 
                      </Select>
                    )}
                    </Form.Item>
                    <Form.Item>
                    {getFieldDecorator('address', {
                        rules: [{ required: true, message: 'Please enter  address' }],
                    })(
                        <Input placeholder={allStrings.address} />
                    )}
                    </Form.Item>
                   
                   
                   
                    </Form>
                </Modal>
              
              </div>
              </div>
          </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
  }


  export default withRouter( connect(mapToStateProps,mapDispatchToProps)  (User = Form.create({ name: 'normal_login' })(User)) );
 


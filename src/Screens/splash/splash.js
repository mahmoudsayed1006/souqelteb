import React from 'react';
import './splash.css';
import "antd/dist/antd.css";

import { connect } from 'react-redux';
import {withRouter} from 'react-router-dom'
import firebase from 'firebase';
import {userToken,getUser} from '../../actions/AuthActions'
import { WhisperSpinner } from "react-spinners-kit";
import  {allStrings} from '../../assets/strings'
import {ChangeLanguage} from '../../actions/LanguageAction'



class Splash extends React.Component {

    state = {
        loading: true,
    };
  

  constructor(props){
    super(props)
    if(this.props.isRTL){
      allStrings.setLanguage('ar')
    }else{
      allStrings.setLanguage('en')
    }
  }
  getFirebaseToken = async () => {
    try {
  
      const messaging = firebase.messaging();
      await messaging.requestPermission();
      const token = await messaging.getToken();
      this.checkToken(token);
      //console.log('user token: ', token);
  
      return token;
    } catch (error) {
      //console.error(error);
    }
  }

  checkToken =  (token) => {
   const t = localStorage.getItem('@QsathaToken')
    if(t){
        this.props.userToken(t);
        //console.log('check token true')
    }else{
      localStorage.setItem('@QsathaToken',token)
        this.props.userToken(token)
        //console.log('check token false')
    }

    //console.log("My Token")
    //console.log(t)
    //console.log(token)
}

checkLogin =  () => {      
  const userJSON = localStorage.getItem('@QsathaUser');
  if(userJSON){
      const userInfo = JSON.parse(userJSON);
      console.log(userInfo)
     this.props.getUser(userInfo);
     this.props.history.push('/Dashboard')    
    }else{
      this.props.history.push('/Login')
    }
}

checkLanguage =  () => {      
  const lang = localStorage.getItem('@lang');
  if(lang){
       allStrings.setLanguage(lang) 
       if(lang==='ar'){
        this.props.ChangeLanguage(true)  
       }else{
        this.props.ChangeLanguage(false)
       }
      
    }else{
      allStrings.setLanguage('ar') 
      this.props.ChangeLanguage(true)  
    }
}


   
     componentDidMount(){
      // console.log("555")
      this.getFirebaseToken()
      this.checkLanguage()
      setTimeout(()=>{this.checkLogin()},5000)
     }
     
    
    render() {
      const { loading } = this.state;
      return (
          <div className='splash'>
             
             <WhisperSpinner
                size={100}
                color="#26a69a"
                loading={loading}
            />
             
          </div>
      );
    }
  }

  
  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
  })
  
  const mapDispatchToProps = {
    userToken,
    getUser,
    ChangeLanguage
  }

  export default  withRouter(connect(mapToStateProps,mapDispatchToProps)(Splash));

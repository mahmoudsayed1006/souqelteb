import React, { Component } from 'react';
import Menu from '../../components/menu/menu';
import AppMenu from '../../components/menu/menu';
import Nav from '../../components/navbar/navbar';
import Footer from '../../components/footer/footer';

import './user info.css';
import {Button,Card,Input,CardTitle} from 'react-materialize';
import "antd/dist/antd.css";
 import { Modal,Form ,Select ,DatePicker,message,Icon} from 'antd';
 import axios from 'axios';
 import {BASE_END_POINT} from '../../config/URL'
import {Table} from 'react-materialize'
import { connect } from 'react-redux'
import  {allStrings} from '../../assets/strings'
import {getUser} from '../../actions/AuthActions'



class UserInfo extends React.Component {
       //submit form
       page=1;
       type=null;
       state = {
        modal1Visible: false,
        user:this.props.location.state.data,
        file:null,
        ordermodal:false,
        Ads:[],
        loading:false,
        selectFlag:false,
        cities:[],
        areas:[],
        orders:[]
        //this.props.location.state.data.img[0],
        
      }

      constructor(props){
        super(props)
        this.getcities()
        this.getorders(1)
        if(this.props.isRTL){
          allStrings.setLanguage('ar')
        }else{
          allStrings.setLanguage('en')
        }
      }
      getorders = (page) => {
        axios.get(`${BASE_END_POINT}orders?client=${this.state.user.id}&page=${page}&limit=20`)
        .then(response=>{
          console.log("ALL orders    ",response.data)
          console.log(response.data.data)
          
          this.setState({orders:response.data.data})
        })
        .catch(error=>{
          console.log("ALL orders ERROR")
          console.log(error.response)
          this.setState({loading:false})
        })
      }
     
    getcities = () => {
      axios.get(`${BASE_END_POINT}cities`)
      .then(response=>{
        console.log(response.data)
        this.setState({cities:response.data})
      })
      .catch(error=>{
        //console.log("ALL Categories ERROR")
        //console.log(error.response)
      })
    }
    getarea = (id) => {
      axios.get(`${BASE_END_POINT}cities/${id}/areas`)
      .then(response=>{
        this.setState({areas:response.data})
      })
      .catch(error=>{
        //console.log("ALL Categories ERROR")
        //console.log(error.response)
      })
    }
   

   

      onChange = (e) => {
        this.setState({file:e.target.files[0]});
    }

       componentDidMount()
       {
          // this.getAds(this.page)
       }

       deleteUser = () => {
        let l = message.loading(allStrings.wait, 2.5)
        axios.delete(`${BASE_END_POINT}${this.state.user.id}/delete`,{
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': `Bearer ${this.props.currentUser.token}`
          },
        })
        .then(response=>{
            l.then(() => message.success(allStrings.deleteDone, 2.5))
            this.props.history.goBack()
        })
        .catch(error=>{
            //console.log(error.response)
            l.then(() => message.error('Error', 2.5))
        })
     }


       block = (active) => {
           let uri ='';
           if(active){
            uri = `${BASE_END_POINT}${this.state.user.id}/block`
           }else{
            uri = `${BASE_END_POINT}${this.state.user.id}/unblock`
           }
          let l = message.loading(allStrings.wait, 2.5)
           axios.put(uri,{},{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
           .then(response=>{
               // console.log('done')
                if(active){
                    
                    
                    l.then(() => message.success(allStrings.blockDone, 2.5))
                    
                }else{
                
                  l.then(() => message.success(allStrings.unblockDone, 2.5))
                }
                this.props.history.goBack()
           })
           .catch(error=>{
           // console.log('Error')
           // console.log(error.response)
            l.then(() => message.error('Error', 2.5))
           })
       }

     

       handleSubmit = (e) => {
        e.preventDefault();
        console.log('user ID     ',this.state.user.id)
        this.props.form.validateFields((err, values) => {
          if (!err) {
            console.log('Received values of form: ', values);
            var form = new FormData();
            if(this.state.file){
                form.append('img',this.state.file);
            }
            form.append('firstname', values.firstname);
            form.append('lastname', values.lastname);
            form.append('email', values.email);
            form.append('phone', values.phone);
            form.append('city', values.city.key);
            form.append('area', values.area.key);
            form.append('type', this.state.user.type);
            form.append('address', values.address);
            let l = message.loading(allStrings.wait, 2.5)
            axios.put(`${BASE_END_POINT}user/${this.state.user.id}/updateInfo`,form,{
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.props.currentUser.token}`
              },
            })
            .then(response=>{
                l.then(() => message.success(allStrings.updatedDone, 2.5));           
                this.setState({ modal1Visible:false });
                this.props.history.goBack()
            })
            .catch(error=>{
                console.log(error.response)
                l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
            })
          }
        });
       
        
      }

    //end submit form
      
      //modal
    
  
      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }
  

  //end modal
  
    render() {
        const { getFieldDecorator } = this.props.form;
        const {user} = this.state;
        const order = this.state.orders;

        const {Ads} = this.state;
         //select
         const Option = Select.Option;

         function handleChange(value) {
            //console.log(value); 
         }
         //end select
         const {select} = this.props;
         let img;
         if(user.img){
          img = user.img
         } else{
           img ='https://image.flaticon.com/icons/png/512/149/149071.png'
         }
 
      return (
          
        <div>
         <AppMenu height={'200%'} goTo={this.props.history} />
        <Nav></Nav>
        <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}} className='menu'>
        <div className='login'>
        <div class="row">
            <div class="col m2">
                <div class='title' style={{backgroundColor:'#001529'}}>
                    <h2 class="center-align" style={{color:'#fff'}}>{allStrings.userinfo}</h2>
                </div>
                <div class="row">
                
                    <form class="col s12">
                    <img style={{borderColor:'transparent'}} src={ img}></img>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="username" type="text" class="validate" disabled value={user.firstname}>
                            </input>
                            <label for="usertname" class="active">{allStrings.firstname}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="email" type="text" class="validate" disabled value={user.lastname}>
                            </input>
                            <label for="email" class="active">{allStrings.lastname}</label>
                            </div>
                        </div>
                        <div class="row">
                        <div class="input-field col s6">
                            <input id="email" type="text" class="validate" disabled value={user.phone}>
                            </input>
                            <label for="email" class="active">{allStrings.phone}</label>
                            </div>
                            <div class="input-field col s6">
                            <input id="phone" type="text" class="validate" disabled value={user.email}></input>
                            <label for="phone" class="active">{allStrings.email}</label>
                            </div>
                        </div>
                       
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="cardNum" type="text" class="validate" disabled value={user.id}>
                            </input>
                            <label for="cardNum" class="active">{allStrings.id}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="cardNum" type="text" class="validate" disabled value={user.block}>
                            </input>
                            <label for="cardNum" class="active">{allStrings.block}</label>
                            </div>
                           
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                            <input id="cardNum" type="text" class="validate" disabled value={this.props.isRTL?user.city.arabicCityName:user.city.cityName}>
                            </input>
                            <label for="cardNum" class="active">{allStrings.city}</label>
                            </div>

                            <div class="input-field col s6">
                            <input id="cardNum" type="text" class="validate" disabled value={this.props.isRTL?user.area.arabicAreaName:user.area.areaName}>
                            </input>
                            <label for="cardNum" class="active">{allStrings.area}</label>
                            </div>
                           
                        </div>
                        <div className='dash-table'>
                           <h5>{allStrings.orders} :</h5>
                           <div className='row'>
                               <div className="col s6 m6 l6 dashboard-table">
                                   <Table>
                                       <thead>
                                           <tr>
                                           <th data-field="id">{allStrings.id}</th>
                                           <th data-field="mobile">{allStrings.finalTotal}</th>
                                           <th data-field="address">{allStrings.city}</th>
                                           <th data-field="name">{allStrings.product}</th>
                                           <th data-field="email">{allStrings.count}</th>
                                           <th data-field="email">{allStrings.status}</th>
                                           <td data-field="email">{allStrings.paymentSystem}</td>
                                           </tr>
                                       </thead>

                                       <tbody>
                                       {this.state.orders.map(val=>(
                                           <tr onClick={()=>{
                                               this.props.history.push('/OrderInfo',{data:val})
                                                }}>
                                               <td>{val.id}</td>
                                               <td>{val.finalTotal}</td>
                                               <td>{val.city.cityName}</td>
                                               <td>
                                               {val.productOrders.map(val2=>(
                                                 <span>{val2.product.name}<br></br></span>
                                                 
                                               ))}
                                               </td>
                                               <td>
                                               {val.productOrders.map(val3=>(
                                                 <span>{val3.count}<br></br></span>
                                               ))}
                                               </td>
                                               <td>{val.status}</td>
                                               <td>{val.paymentSystem}</td>
                                               
                                           </tr>
                                       ))}
                                       </tbody>
                                   </Table>
                               </div>
                           </div>
                       </div>
                      
                       

                                
                        <div>
                        <a class="waves-effect waves-light btn btn-large delete"  onClick={()=>this.deleteUser()} ><i class="spcial material-icons left">delete</i>{allStrings.remove}</a>
                        <a class="waves-effect waves-light btn btn-large edit" onClick={() => this.block(true)}><i class="material-icons left spcial">block</i>{allStrings.block}</a>
                        <a class="waves-effect waves-light btn btn-large edit" onClick={() => this.block(false)}><i class="material-icons left spcial">remove</i>{allStrings.unblock}</a>                 
                        <a class="waves-effect waves-light btn btn-large edit" onClick={() => this.setModal1Visible(true)}><i class=" spcial material-icons left">edit</i>{allStrings.edit}</a>
                        </div>
                       
                                                                             
                        </form>
                    <Modal
                            title="Edit"
                            visible={this.state.modal1Visible}
                            onOk={this.handleSubmit}
                            okText={allStrings.ok}
                            cancelText={allStrings.cancel}
                            onCancel={() => this.setModal1Visible(false)}
                        >
                            
                            <Form onSubmit={this.handleSubmit} className="login-form">

                             <label for="name" class="lab">{allStrings.firstname}</label>
                            <Form.Item>
                            {getFieldDecorator('firstname', {
                                rules: [{ required: true, message: 'Please enter first name' }],
                                initialValue: user.firstname,
                            })(
                                <Input/>
                            )}
                            </Form.Item>
                            <label for="name" class="lab">{allStrings.lastname}</label>
                            <Form.Item>
                            {getFieldDecorator('lastname', {
                                rules: [{ required: true, message: 'Please enter last name' }],
                                initialValue: user.lastname,
                            })(
                                <Input/>
                            )}
                            </Form.Item>

                            <label for="name" class="lab">{allStrings.email}</label>
                            <Form.Item>
                            {getFieldDecorator('email', {
                                rules: [{ required: true, message: 'Please enter email' }],
                                initialValue:user.email
                            })(
                                <Input />
                            )}
                            </Form.Item>
                            <label for="name" class="lab">{allStrings.city}</label>

                            <Form.Item>
                            {getFieldDecorator('city', {
                                rules: [{ required: true, message: 'Please enter city' }],
                            })(
                              <Select labelInValue  
                              placeholder={allStrings.city}
                              style={{ width: '100%'}} >
                                  {this.state.cities.map(val=>
                                  <Option onClick={()=>{this.getarea(val.id)}} value={val.id}>{this.props.isRTL?val.arabicCityName:val.cityName}</Option>
                                  )}                 
                              </Select>
                            )}
                            </Form.Item>
                            <label for="name" class="lab">{allStrings.area}</label>

                            <Form.Item>
                            {getFieldDecorator('area', {
                                rules: [{ required: true, message: 'Please enter area' }],
                            })(
                              <Select labelInValue  
                              placeholder={allStrings.area}
                              style={{ width: '100%'}} >
                                  {this.state.areas.map(val=>
                                  <Option value={val.id}>{this.props.isRTL?val.arabicAreaName:val.areaName}</Option>
                                  )}                 
                              </Select>
                            )}
                            </Form.Item>
                                  
                                
                            <label for="name" class="lab">{allStrings.phone}</label>
                            <Form.Item>
                            {getFieldDecorator('phone', {
                                rules: [{ required: true, message: 'Please enter phone' }],
                                initialValue:user.phone
                            })(
                                <Input/>
                            )}
                            </Form.Item>
                            <label for="name" class="lab">{allStrings.address}</label>
                            <Form.Item>
                            {getFieldDecorator('address', {
                                rules: [{ required: true, message: 'Please enter address' }],
                                initialValue:user.address
                            })(
                                <Input/>
                            )}
                            </Form.Item>


                            </Form>
                            <label for="name" class="lab">{allStrings.personalImage}</label>
                            <br/>
                            <input className='profileImg' type="file" onChange= {this.onChange}></input>
                        </Modal>
                             
                    </div>
            </div>
        </div>
        </div>
        <Footer></Footer>
        </div>
    </div>
      );
    }
  }

  
  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,
  })
  
  const mapDispatchToProps = {
    getUser,
  }


export default connect(mapToStateProps,mapDispatchToProps) ( UserInfo = Form.create({ name: 'normal_login' })(UserInfo)) ;

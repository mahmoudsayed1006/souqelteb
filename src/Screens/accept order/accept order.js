import React from 'react';
import AppMenu from '../../components/menu/menu';

import Nav from '../../components/navbar/navbar';
import Tables from '../../components/table/table';
import Footer from '../../components/footer/footer';

import './accept order.css';
import { Skeleton, message,Form, Icon,Popconfirm,Button,DatePicker} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'
const { RangePicker} = DatePicker;
class AcceptedOrder extends React.Component {
  page = 0;
  pagentationPage=0;
  counter=0;
  state = {
    modal1Visible: false,
    confirmDelete: false,
    selectedAcceptedOrder:null,
     AcceptedOrders:[],
     parents:[],
     file:null,
     loading:true,
     tablePage:0,
     start:null,
     end:null
     }

     constructor(props){
      super(props)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
      this.dateChange = this.dateChange.bind(this);
    }

     onChange = (e) => {
      this.setState({file:e.target.files[0]});
     
  }
    //submit form
    flag = 0;
    getAcceptedOrders = (page,deleteRow) => {
      axios.get(`${BASE_END_POINT}orders?status=ACCEPTED&page=${page}&limit=20`)
      .then(response=>{
        console.log("ALL AcceptedOrders    ",response.data)
        console.log(response.data)
        if(deleteRow){
          this.setState({tablePage:0})
         }
        this.setState({AcceptedOrders:deleteRow?response.data.data:[...this.state.AcceptedOrders,...response.data.data],loading:false})
      })
      .catch(error=>{
        console.log("ALL AcceptedOrders ERROR")
        console.log(error.response)
        this.setState({loading:false})
      })
    }
    getordersFilter = (page,deleteRow) => {
      axios.get(`${BASE_END_POINT}orders?status=ACCEPTED&start=${this.state.start}&end=${this.state.end}&page=${page}&limit=20`)
      .then(response=>{
        console.log("ALL orders    ",response.data)
        console.log(response.data)
        if(deleteRow){
          this.setState({tablePage:0})
         }
        this.setState({AcceptedOrders:deleteRow?response.data.data:[...this.state.AcceptedOrders,...response.data.data],loading:false})

      })
      .catch(error=>{
        this.setState({loading:false})
        console.log("ALL orders ERROR")
        console.log(error.response)
        
      })
    }
    
    componentDidMount(){
      this.getAcceptedOrders(1,true)
    }
    OKBUTTON = (e) => {
      this.deleteAcceptedOrder()
     }

     deleteAcceptedOrder = () => {
       let l = message.loading(allStrings.wait, 2.5)
       axios.delete(`${BASE_END_POINT}orders/${this.state.selectedAcceptedOrder}`,{
         headers: {
           'Content-Type': 'application/json',
           'Authorization': `Bearer ${this.props.currentUser.token}`
         },
       })
       .then(response=>{
           l.then(() => message.success(allStrings.deleteDone, 2.5))
           this.getAcceptedOrders(1,true)
           this.flag = -1
       })
       .catch(error=>{
          // console.log(error.response)
           l.then(() => message.error('Error', 2.5))
       })
    }
    dateChange(data,dateString) {
      console.log(dateString);
      this.setState({ start:dateString[0],end:dateString[1] });
      
    }
     
    //end submit form

      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }

//end modal
    render() {
        //form
         const { getFieldDecorator } = this.props.form;
       
          let controls = (
            <Popconfirm
            title={allStrings.areYouSure}
            onConfirm={this.OKBUTTON}
            onCancel={this.fCANCELBUTTON}
            okText={allStrings.ok}
            cancelText={allStrings.cancel}
          >
             <Icon className='controller-icon' type="delete" />
          </Popconfirm>
         )
        
         let list = this.state.AcceptedOrders.map((val,index)=>{
        
          return  [
            val.id,val.client.firstname+" "+val.client.lastname,
            val.productOrders.map(v=><h6>{v.product.name}</h6>),
            val.productOrders.map(v=><h6>{v.count}</h6>),
            val.total, 
            val.delivaryCost,
            val.finalTotal,
            val.city.cityName, 
            val.createdAt.substring(0, 10),
            controls
            ]
          })     

          const loadingView = [
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            
           ]
           const {select} = this.props;
      return (
          <div>
              <AppMenu height={'140%'} goTo={this.props.history}></AppMenu>
              <Nav></Nav>
              <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}} className='menu'>
              <div className='filterCustom'>
              <div class="row"> 
                <div class="input-field col s12 filterCustom" style={{width: '90%', marginRight:'2%' }}>
                  
                <RangePicker size='large' onChange={this.dateChange}  id="searchCompany" style={{width: '91%', marginRight:'2rem',marginLeft:'2rem'}} />
                  <Button style={{color: 'white', backgroundColor:'#3497fd', width: '85%', marginRight:'1rem',marginLeft:'1rem',height:'40px'}} onClick={() => this.getordersFilter(1,true)}>{allStrings.search}</Button>
                  <Icon className='controller-icon' type="undo" onClick={() => this.getAcceptedOrders(1,true)} />
                </div>
              </div>
              </div>
              <Tables
               columns={this.state.loading?['loading...']: [allStrings.id,allStrings.client,allStrings.product,allStrings.count,allStrings.total,allStrings.delivaryCost,allStrings.finalTotal,allStrings.city,allStrings.date,allStrings.remove]} 
              title={allStrings.acceptOrders}
              page={this.state.tablePage}
              onCellClick={(colData,cellMeta,)=>{
                //console.log('col index  '+cellMeta.colIndex)
                //console.log('row index   '+colData)
                if(cellMeta.colIndex!==9){
                  
                  //console.log(this.state.AcceptedOrders[cellMeta.rowIndex])
                  this.props.history.push('/OrderInfo',{data:this.state.AcceptedOrders[this.pagentationPage+cellMeta.rowIndex]})
                }else if(cellMeta.colIndex===9){
                    const id = list[ this.pagentationPage+cellMeta.rowIndex][0];
                    this.setState({selectedAcceptedOrder:id})
                    //console.log(id)
                  }
              }}
               onChangePage={(currentPage)=>{
                this.setState({tablePage:currentPage})
                if(currentPage>this.counter){
                  this.counter=currentPage;
                  this.pagentationPage=this.pagentationPage+10
                }else{
                 this.counter=currentPage;
                 this.pagentationPage=this.pagentationPage-10
                }
                //console.log(currentPage)
                if(currentPage%2!=0  && currentPage > this.flag){
                  let rem = currentPage %2;
                  let x = currentPage - rem;
                  console.log(rem)
                  console.log(x)
                  this.flag  = this.flag +1;
                  console.log("flage",this.flag)
                    let page = rem + this.flag;
                    console.log("page",page)
                    this.getAcceptedOrders(page)
                    console.log("request",page)
                    console.log("flag",this.flag) 

                } 
                  
              }}
              arr={this.state.loading?loadingView:list}></Tables>
            </div>
             <Footer></Footer>
          </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(AcceptedOrder = Form.create({ name: 'normal_login' })(AcceptedOrder));

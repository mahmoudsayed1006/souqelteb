import React from 'react';
import AppMenu from '../../components/menu/menu';

import Nav from '../../components/navbar/navbar';
import Tables from '../../components/table/table';
import Footer from '../../components/footer/footer';

import './size.css';
import { Skeleton, message,Modal, Form, Icon, Input, Button,Popconfirm} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'


class Size extends React.Component {
  page = 0;
  pagentationPage=0;
  counter=0;
  state = {
    modal1Visible: false,
    confirmDelete: false,
    selectedSize:null,
     Sizes:[],
     parents:[],
     file:null,
     loading:true,
     tablePage:0,
     }

     constructor(props){
      super(props)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }

     onChange = (e) => {
      this.setState({file:e.target.files[0]});
     
  }
    //submit form
    flag = -1;
    getSizes = (page,deleteRow) => {
      axios.get(`${BASE_END_POINT}size?page=${page}&limit=20`)
      .then(response=>{
        console.log("ALL Sizes    ",response.data.data)
        console.log(response.data)
        if(deleteRow){
          this.setState({tablePage:0})
         }
         this.setState({Sizes:deleteRow?response.data.data:[...this.state.Sizes,...response.data.data],loading:false})
        })
      .catch(error=>{
        console.log("ALL Sizes ERROR")
        console.log(error.response)
        this.setState({loading:false})
      })
    }
    
    componentDidMount(){
      this.getSizes(1,true)
    }
    OKBUTTON = (e) => {
      this.deleteSize()
     }

     deleteSize = () => {
       let l = message.loading(allStrings.wait, 2.5)
       axios.delete(`${BASE_END_POINT}size/${this.state.selectedSize}`,{
         headers: {
           'Content-Type': 'application/json',
           'Authorization': `Bearer ${this.props.currentUser.token}`
         },
       })
       .then(response=>{
           l.then(() => message.success(allStrings.deleteDone, 2.5))
           this.getSizes(1,true)
           this.flag = -1
       })
       .catch(error=>{
          // console.log(error.response)
           l.then(() => message.error('Error', 2.5))
       })
    }
    handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          //console.log('date ', date[2]  );
          const data = {
              size: values.size,
              arabicSize: values.arabicSize,
              sizeFrom:values.sizeFrom,
            sizeTo:values.sizeTo,
            price:values.price
             
                                                         
          }
        
          let l = message.loading(allStrings.wait, 2.5)
          axios.post(`${BASE_END_POINT}size`,JSON.stringify(data),{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
          .then(response=>{
            console.log(allStrings.addDone)
              l.then(() => message.success('Add size', 2.5));
              this.setState({ modal1Visible:false });
              this.getSizes(1,true)
              this.flag = -1
              this.props.form.resetFields() 
          })
          .catch(error=>{
            console.log("Add size Error")
              console.log(error.response)
              l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
          })

        }
      });
      
    }
     
    //end submit form

      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }

//end modal
    render() {
        //form
         const { getFieldDecorator } = this.props.form;
       
          let controls = (
            <Popconfirm
            title={allStrings.areYouSure}
            onConfirm={this.OKBUTTON}
            onCancel={this.fCANCELBUTTON}
            okText={allStrings.ok}
            cancelText={allStrings.cancel}
          >
             <Icon className='controller-icon' type="delete" />
          </Popconfirm>
         )
         let list =this.state.Sizes.map((val,index)=>[
          val.id,val.size,val.arabicSize,val.sizeFrom,val.sizeTo,val.price,
          controls
        ])
         

          const loadingView = [
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            
           ]
           const {select} = this.props;
      return (
          <div>
              <AppMenu height={'140%'} goTo={this.props.history}></AppMenu>
              <Nav></Nav>
              <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}} className='menu'>
              <Tables
               columns={this.state.loading?['loading...']: [allStrings.id,allStrings.EnglishSizeName,allStrings.arabicSizeName,allStrings.from ,allStrings.to,allStrings.delivaryCost,allStrings.remove]} 
              title={allStrings.SizeTable}
              page={this.state.tablePage}
              onCellClick={(colData,cellMeta,)=>{
                //console.log('col index  '+cellMeta.colIndex)
                //console.log('row index   '+colData)
                if(cellMeta.colIndex!==6){
                  
                  //console.log(this.state.countries[cellMeta.rowIndex])
                  this.props.history.push('/SizeInfo',{data:this.state.Sizes[this.pagentationPage+cellMeta.rowIndex]})
                }else if(cellMeta.colIndex===6){
                    const id = list[ this.pagentationPage+cellMeta.rowIndex][0];
                    this.setState({selectedSize:id})
                    //console.log(id)
                  }
              }}
               onChangePage={(currentPage)=>{
                this.setState({tablePage:currentPage})
                if(currentPage>this.counter){
                  this.counter=currentPage;
                  this.pagentationPage=this.pagentationPage+10
                }else{
                 this.counter=currentPage;
                 this.pagentationPage=this.pagentationPage-10
                }
                //console.log(currentPage)
                if(currentPage%2!==0  && currentPage > this.flag){
                  this.getSizes(currentPage+1)
                  this.flag  = currentPage;
                 
                }
                  
              }}
              arr={this.state.loading?loadingView:list}></Tables>
              <div>
              <Button style={{color: 'white', backgroundColor:'#3497fd', marginLeft:60}} onClick={() => this.setModal1Visible(true)}>{allStrings.addSize}</Button>
              <Modal
                    title={allStrings.addSize}
                    visible={this.state.modal1Visible}
                    onOk={this.handleSubmit}
                    onCancel={() => this.setModal1Visible(false)}
                    okText={allStrings.ok}
                    cancelText={allStrings.cancel}
                  >
                    <Form onSubmit={this.handleSubmit} className="login-form">
                        <Form.Item>
                        {getFieldDecorator('size', {
                            rules: [{ required: true, message: 'Please enter english size' }],
                        })(
                            <Input placeholder={allStrings.EnglishSizeName}/>
                        )}
                        </Form.Item>
                        <Form.Item>
                        {getFieldDecorator('arabicSize', {
                            rules: [{ required: true, message: 'Please enter arabic Size' }],
                        })(
                            <Input placeholder={allStrings.arabicSizeName}/>
                        )}
                        </Form.Item>
                        <Form.Item>
                          {getFieldDecorator('sizeFrom', {
                              rules: [{ required: true, message: 'Please enter low size' }],
                          })(
                              <Input  placeholder={allStrings.from}/>
                          )}
                          </Form.Item>
                          <Form.Item>
                          {getFieldDecorator('sizeTo', {
                              rules: [{ required: true, message: 'Please enter higher size' }],
                          })(
                              <Input  placeholder={allStrings.to}/>
                          )}
                          </Form.Item>
                          <Form.Item>
                          {getFieldDecorator('price', {
                              rules: [{ required: true, message: 'Please enter dilivery cost' }],
                          })(
                              <Input  placeholder={allStrings.delivaryCost}/>
                          )}
                          </Form.Item>

                        
                    </Form>
                </Modal>
              </div> 
            </div>
             <Footer></Footer>
          </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(Size = Form.create({ name: 'normal_login' })(Size));

import React from 'react';
import AppMenu from '../../components/menu/menu';

import Nav from '../../components/navbar/navbar';
import Tables from '../../components/table/table';
import Footer from '../../components/footer/footer';

import './city.css';
import { Skeleton, message,Modal, Form, Icon, Input, Button,Popconfirm} from 'antd';
import "antd/dist/antd.css";
import axios from 'axios';
import {BASE_END_POINT} from '../../config/URL'
import { connect } from 'react-redux';
import  {allStrings} from '../../assets/strings'


class City extends React.Component {
  page = 0;
  pagentationPage=0;
  counter=0;
  state = {
    modal1Visible: false,
    confirmDelete: false,
    selectedCity:null,
     cities:[],
     parents:[],
     file:null,
     loading:true,
     tablePage:0,
     }

     constructor(props){
      super(props)
      if(this.props.isRTL){
        allStrings.setLanguage('ar')
      }else{
        allStrings.setLanguage('en')
      }
    }

     onChange = (e) => {
      this.setState({file:e.target.files[0]});
     
  }
    //submit form
    flag = -1;
    getcities = (page,deleteRow) => {
      axios.get(`${BASE_END_POINT}cities?page=${page}&limit=20`)
      .then(response=>{
        console.log("ALL cities    ",response.data)
        console.log(response.data)
        if(deleteRow){
          this.setState({tablePage:0})
         }
        this.setState({cities:deleteRow?response.data:[...this.state.cities,...response.data],loading:false})
      })
      .catch(error=>{
        console.log("ALL cities ERROR")
        console.log(error.response)
        this.setState({loading:false})
      })
    }
    
    componentDidMount(){
      this.getcities(1,true)
    }
    OKBUTTON = (e) => {
      this.deleteCity()
     }

     deleteCity = () => {
       let l = message.loading(allStrings.wait, 2.5)
       axios.delete(`${BASE_END_POINT}cities/${this.state.selectedCity}`,{
         headers: {
           'Content-Type': 'application/json',
           'Authorization': `Bearer ${this.props.currentUser.token}`
         },
       })
       .then(response=>{
           l.then(() => message.success(allStrings.deleteDone, 2.5))
           this.getcities(1,true)
           this.flag = -1
       })
       .catch(error=>{
          // console.log(error.response)
           l.then(() => message.error('Error', 2.5))
       })
    }
    handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          //console.log('date ', date[2]  );
          const data = {
            cityName:values.cityName,
            arabicCityName:values.arabicName,
            delivaryCost:values.delivaryCost,                             
          }
        
          let l = message.loading(allStrings.wait, 2.5)
          axios.post(`${BASE_END_POINT}cities`,JSON.stringify(data),{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.props.currentUser.token}`
            },
          })
          .then(response=>{
            console.log(allStrings.addDone)
              l.then(() => message.success('Add city', 2.5));
              this.setState({ modal1Visible:false });
              this.getcities(1,true)
              this.flag = -1
              this.props.form.resetFields() 
          })
          .catch(error=>{
            console.log("Add city  Error")
              console.log(error.response)
              l.then(() => message.error(error.response.data.errors[0].msg, 2.5))
          })

        }
      });
      
    }
 
     
    //end submit form

      setModal1Visible(modal1Visible) {
        this.setState({ modal1Visible });
      }

//end modal
    render() {
        //form
         const { getFieldDecorator } = this.props.form;
       
          let controls = (
            <Popconfirm
            title={allStrings.areYouSure}
            onConfirm={this.OKBUTTON}
            onCancel={this.fCANCELBUTTON}
            okText={allStrings.ok}
            cancelText={allStrings.cancel}
          >
             <Icon className='controller-icon' type="delete" />
          </Popconfirm>
         )
        
          let list =this.state.cities.map((val,index)=>[
            val.id,val.cityName,val.arabicCityName,
           ""+val.delivaryCost,controls
          ])

          const loadingView = [
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            [<Skeleton  active/> ],
            [<Skeleton active/> ],
            
           ]
           const {select} = this.props;
      return (
          <div>
              <AppMenu height={'140%'} goTo={this.props.history}></AppMenu>
              <Nav></Nav>
              <div style={{marginRight:!select?'20.2%':'5.5%',borderWidth:2}} className='menu'>
              <Tables
               columns={this.state.loading?['loading...']: [allStrings.id,allStrings.EnglishCityName,allStrings.ArabicCityName,allStrings.delivaryCost, allStrings.remove]} 
              title={allStrings.CityTable}
              page={this.state.tablePage}
              onCellClick={(colData,cellMeta,)=>{
                //console.log('col index  '+cellMeta.colIndex)
                //console.log('row index   '+colData)
                if(cellMeta.colIndex!==4){
                  
                  //console.log(this.state.cities[cellMeta.rowIndex])
                  this.props.history.push('/CityInfo',{data:this.state.cities[this.pagentationPage+cellMeta.rowIndex]})
                }else if(cellMeta.colIndex===4){
                    const id = list[ this.pagentationPage+cellMeta.rowIndex][0];
                    this.setState({selectedCity:id})
                    //console.log(id)
                  }
              }}
               onChangePage={(currentPage)=>{
                this.setState({tablePage:currentPage})
                if(currentPage>this.counter){
                  this.counter=currentPage;
                  this.pagentationPage=this.pagentationPage+10
                }else{
                 this.counter=currentPage;
                 this.pagentationPage=this.pagentationPage-10
                }
                //console.log(currentPage)
                if(currentPage%2!==0  && currentPage > this.flag){
                  this.getcities(currentPage+1)
                  this.flag  = currentPage;
                 
                }
                  
              }}
              arr={this.state.loading?loadingView:list}></Tables>
              <div>
              <Button style={{color: 'white', backgroundColor:'#3497fd', marginLeft:60}} onClick={() => this.setModal1Visible(true)}>{allStrings.addCity}</Button>
              <Modal
                    title={allStrings.addCity}
                    visible={this.state.modal1Visible}
                    onOk={this.handleSubmit}
                    onCancel={() => this.setModal1Visible(false)}
                    okText={allStrings.ok}
                    cancelText={allStrings.cancel}
                  >
                    <Form onSubmit={this.handleSubmit} className="login-form">
                        <Form.Item>
                        {getFieldDecorator('cityName', {
                            rules: [{ required: true, message: 'Please enter City name' }],
                        })(
                            <Input placeholder={allStrings.EnglishCityName}/>
                        )}
                        </Form.Item>

                        <Form.Item>
                        {getFieldDecorator('arabicName', {
                            rules: [{ required: true, message: 'Please enter arabic City name' }],
                        })(
                            <Input placeholder={allStrings.ArabicCityName}/>
                        )}
                        </Form.Item>


                       

                        <Form.Item>
                        {getFieldDecorator('delivaryCost', {
                            rules: [{ required: true, message: 'Please enter arabic delivaryCost' }],
                        })(
                            <Input placeholder={allStrings.delivaryCost}/>
                        )}
                        </Form.Item>

                        
                        
                    </Form>
                </Modal>
              </div> 
            </div>
             <Footer></Footer>
          </div>
      );
    }
  }

  const mapToStateProps = state => ({
    isRTL: state.lang.isRTL,
    currentUser: state.auth.currentUser,
    select: state.menu.select,

  })
  
  const mapDispatchToProps = {
  }

  export default  connect(mapToStateProps,mapDispatchToProps)(City = Form.create({ name: 'normal_login' })(City));

import React, { Component } from 'react';
import './menu.css';
import { connect } from 'react-redux'
import  {allStrings} from '../../assets/strings'
//import {Button, SideNav ,SideNavItem,Icon,Collapsible,CollapsibleItem} from 'react-materialize'
import { Menu, Icon} from 'antd';
import "antd/dist/antd.css";
import {SelectMenuItem } from '../../actions/MenuAction'

const { SubMenu } = Menu;

class AppMenu extends Component {
  constructor(props){
    super(props)
    if(this.props.isRTL){
      allStrings.setLanguage('ar')
    }else{
      allStrings.setLanguage('en')
    }
  }

  state = {
    collapsed: false,
  }

  toggleCollapsed = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  render() {
     const {goTo,height} = this.props;
     console.log("GO TO   ",goTo)
    const x  = !this.props.select;
    return (
      <div style={{direction:"rtl"}} >
    
         <Menu  
         style={{position:'fixed',background:'#3497fd'}}       
          //defaultSelectedKeys={[this.props.key]}
          //defaultOpenKeys={['sub1']}
          mode="inline"
          theme="dark"
          inlineCollapsed={this.props.select}
        >
          <Menu.Item  style={{marginTop: "14px"}}
              onClick={()=>{
               
              this.props.SelectMenuItem(0,x)
              }} >
              <Icon type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'} />
              
          </Menu.Item>

          <Menu.Item
            style={{}}
            onClick={()=>{
              goTo.push('/Dashboard')
              //alert("")
              console.log("GO TO   ",goTo)
              this.props.SelectMenuItem(1,true)
            }}
            key="1" style={{marginBottom:6}}>
            <Icon type="home"   style={{fontSize:20,color:'#fff'}} />
              <span  > {allStrings.home}</span>
          </Menu.Item>
                 

          <SubMenu
            key="sub1"
            title={
              <span>
                <Icon  type="usergroup-add" style={{fontSize:20,color:'#fff'}}/>
                <span> {allStrings.users}</span>
              </span>
            }
          >
              <Menu.Item
              onClick={()=>{
                //alert("")
                goTo.push('/users')
                console.log("GO TO   ",goTo)
                this.props.SelectMenuItem(2,true)
              }}
              key="2" style={{marginBottom:6}}>
              <Icon type="usergroup-add" style={{fontSize:20,color:'#fff'}} />
                <span  > {allStrings.users}</span>
              </Menu.Item>
              <Menu.Item
              onClick={()=>{
                //alert("")
                goTo.push('/Admins')
                console.log("GO TO   ",goTo)
                this.props.SelectMenuItem(3,true)
              }}
              key="3" style={{marginBottom:6}} >
                <Icon type="user-delete" style={{fontSize:20,color:'#fff'}} />
                <span> {allStrings.admin}</span>
              </Menu.Item>

          </SubMenu>
          <Menu.Item
              onClick={()=>{
                goTo.push('/category')
                //alert("")
                console.log("GO TO   ",goTo)
                this.props.SelectMenuItem('7',true)
              }}
              key="70" style={{marginBottom:6}}>        
              <Icon type="appstore"   style={{fontSize:20,color:'#fff'}} />
                <span  > {allStrings.category}</span>
          </Menu.Item>


       

        
        <Menu.Item
          onClick={()=>{
            goTo.push('/announcement')
            //alert("")
            console.log("GO TO   ",goTo)
            this.props.SelectMenuItem('20',true)
          }}
          key="33" style={{marginBottom:6}}> 
                 
          <Icon type="desktop"   style={{fontSize:20,color:'#fff'}} />
            <span  > {allStrings.announcement}</span>
        </Menu.Item>
     

        <Menu.Item
            onClick={()=>{
              goTo.push('/cities')
              //alert("")
              console.log("GO TO   ",goTo)
              this.props.SelectMenuItem('21',true)
            }}
            key="44" style={{marginBottom:6}}>        
            <Icon type="flag"    style={{fontSize:20,color:'#fff'}} />
              <span  > {allStrings.cities}</span>
          </Menu.Item>

          <Menu.Item
            onClick={()=>{
              goTo.push('/sizes')
              //alert("")
              console.log("GO TO   ",goTo)
              this.props.SelectMenuItem('21',true)
            }}
            key="440" style={{marginBottom:6}}>  
                  
            <Icon type="font-size"    style={{fontSize:20,color:'#fff'}} />
              <span  > {allStrings.sizes}</span>
          </Menu.Item>
         
          <SubMenu
            key="sub8"
            title={
              <span>
                <Icon  type="carry-out" style={{fontSize:20,color:'#fff'}}/>
                <span> {allStrings.product}</span>
              </span>
            }
          >
              <Menu.Item
              onClick={()=>{
                //alert("")
                goTo.push('/Products')
                console.log("GO TO   ",goTo)
                this.props.SelectMenuItem(23,true)
              }}
              key="2" style={{marginBottom:6}}>
              <Icon type="carry-out"    style={{fontSize:20,color:'#fff'}} />
                <span  > {allStrings.product}</span>
              </Menu.Item>
              <Menu.Item
              onClick={()=>{
                //alert("")
                goTo.push('/topSellerProduct')
                console.log("GO TO   ",goTo)
                this.props.SelectMenuItem(3,true)
              }}
              key="3" style={{marginBottom:6}} >
               <Icon type="rise" style={{fontSize:20,color:'#fff'}} />
                <span> {allStrings.topSeller}</span>
              </Menu.Item>

          </SubMenu>

          <SubMenu
            key="sub4"
            title={
              <span>
                <Icon  type="shopping" style={{fontSize:20,color:'#fff'}}/>
                <span> {allStrings.orders}</span>
              </span>
            }
          >
              <Menu.Item
              onClick={()=>{
                //alert("")
                goTo.push('/AcceptedOrders')
                console.log("GO TO   ",goTo)
                this.props.SelectMenuItem(2,true)
              }}
              key="44" style={{marginBottom:6}}>
              <Icon type="check" style={{fontSize:20,color:'#fff'}} />
                <span  > {allStrings.acceptOrders}</span>
              </Menu.Item>
              <Menu.Item
              onClick={()=>{
                //alert("")
                goTo.push('/RefusedOrders')
                console.log("GO TO   ",goTo)
                this.props.SelectMenuItem(3,true)
              }}
              key="34" style={{marginBottom:6}} >
                <Icon type="stop" style={{fontSize:20,color:'#fff'}} />
                <span> {allStrings.refusedOrders}</span>
              </Menu.Item>
              <Menu.Item
              onClick={()=>{
                //alert("")
                goTo.push('/PendingOrders')
                console.log("GO TO   ",goTo)
                this.props.SelectMenuItem(3,true)
              }}
              key="33" style={{marginBottom:6}} >
                <Icon type="loading" style={{fontSize:20,color:'#fff'}} />
                <span> {allStrings.pendingOrder}</span>
              </Menu.Item>
              <Menu.Item
              onClick={()=>{
                //alert("")
                goTo.push('/DeliverdOrders')
                console.log("GO TO   ",goTo)
                this.props.SelectMenuItem(3,true)
              }}
              key="3" style={{marginBottom:6}} >
                <Icon type="check-circle" style={{fontSize:20,color:'#fff'}} />
                <span> {allStrings.deliverdOrder}</span>
              </Menu.Item>

          </SubMenu>


      
       
        <SubMenu
            key="sub12"
            title={
              <span >
                <Icon  type="container" />
                <span> {allStrings.terms}</span>
              </span>
            }
          >
          <Menu.Item
        onClick={()=>{
          goTo.push('/terms')
          //alert("")
          console.log("GO TO   ",goTo)
          this.props.SelectMenuItem('20',true)
        }}
         key="11" style={{marginBottom:6}}>        
        <Icon type="container"   style={{fontSize:20,color:'#fff'}} />
          <span  > {allStrings.terms}</span>
        </Menu.Item>

        <Menu.Item
        onClick={()=>{
          goTo.push('/about')
          //alert("")
          console.log("GO TO   ",goTo)
          this.props.SelectMenuItem('20',true)
        }}
         key="12" style={{marginBottom:6}}>        
        <Icon type="container"   style={{fontSize:20,color:'#fff'}} />
          <span  > {allStrings.about}</span>
        </Menu.Item>

        <Menu.Item
        onClick={()=>{
          goTo.push('/privacy')
          //alert("")
          console.log("GO TO   ",goTo)
          this.props.SelectMenuItem('20',true)
        }}
         key="13" style={{marginBottom:6}}>        
        <Icon type="container"   style={{fontSize:20,color:'#fff'}} />
          <span  > {allStrings.privacy}</span>
        </Menu.Item>

        <Menu.Item
        onClick={()=>{
          goTo.push('/Conditions')
          //alert("")
          console.log("GO TO   ",goTo)
          this.props.SelectMenuItem('20',true)
        }}
         key="14" style={{marginBottom:6}}>        
        <Icon type="container"   style={{fontSize:20,color:'#fff'}} />
          <span  > {allStrings.conditions}</span>
        </Menu.Item>
        
        
   
          </SubMenu>

        <Menu.Item 
          onClick={()=>{
            //alert("")
            goTo.push('/Contactus')
            console.log("GO TO   ",goTo)
            this.props.SelectMenuItem(5,true)
          }}
          key="5"style={{marginBottom:6}}>
            <Icon type="wechat"  style={{fontSize:20,color:'#fff'}}/>
            <span> {allStrings.contactUS}</span>
          </Menu.Item>
          <Menu.Item
            onClick={()=>{
              goTo.push('/website')
              //alert("")
              console.log("GO TO   ",goTo)
              this.props.SelectMenuItem('234',true)
            }}
            key="322" style={{marginBottom:6}}> 
                  
            <Icon type="ie"   style={{fontSize:20,color:'#fff'}} />
              <span  > {allStrings.website}</span>
          </Menu.Item>


          <Menu.Item 
           onClick={()=>{
            //alert("")
            goTo.push('/reports')
            console.log("GO TO   ",goTo)
            this.props.SelectMenuItem(6,true)
          }}
          key="6" style={{marginBottom:6}}>
            <Icon type="ordered-list" style={{fontSize:20,color:'#fff'}} />
            <span> {allStrings.reports}</span>
          </Menu.Item>

        
        

          
        </Menu>

      </div>

    );
  }
}
const mapToStateProps = state => ({
  isRTL: state.lang.isRTL,
  currentUser: state.auth.currentUser,
  key: state.menu.key,
  select: state.menu.select
})

const mapDispatchToProps = {
  SelectMenuItem,
}

export default connect(mapToStateProps,mapDispatchToProps) (AppMenu);
